#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <errno.h>
#include <math.h>
#include "EasyPIO.h"

#define FD_STDIN 0
#define format9 "\e[30;47m"	// Fondo
#define format10 "\e[97;100m"	// Cuadro
#define format11 "\e[91;107m"	// Seleccionado
#define format12 "\e[30;40m"	// Sombra

extern float tiempo_min;	// Tiempo mínimo en la secuencia de luces, extern para que sea global a todo el proyecto
extern float tiempo_max;	// Tiempo máximo en la secuencia de luces, extern para que sea global a todo el proyecto
extern int tDelays [8];		// Tiempo de cada una de las secuencias de luces
extern float porcentajes[8];	// Porcentajes de velocidad de cada una de las secuencias de luces

char getChar4(){			// Función actualizada de getchar para que no sea bloqueante
	char buff[2];		// No alcanza con solo modificar la configuración de termios
	int l = read(STDIN_FILENO, buff, 1);	// porque solo con eso no toma caracteres del buffer de entrada
	if(l>0)					// gits.github.com/whyrusleeping/3983293
		return buff[0];
	return(EOF);
}

void autoFantasticoRemoto(){
	float porcentaje = porcentajes[0];	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	int tDelay = tDelays[0]; 		// Se carga el tiempo con el valor inicializado con el potenciómetro
	char input = 0;
	char inputRasp = 0;
	int aux1 = 0;				// Arranca en 0 para mostrar la pantalla la primera vez, cuando termina la primera pasada
						// se pone en 1 para que solo se refresque cuando se oprime una tecla
	int aux2 = 0;				// Sirve para que al cambiar de velocidad, el cambio ocurra en la próxima "prendida" o "apagado" de las luces
	int aux3 = 1;				// Indica si el contador debe setearse con el valor inicial o que proviene como resultado de modoficar la velocidad
	int contador = tDelay;
	char command[24];	
	int aux4 = 0;

	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");

	apagarLuces();

	/* Se envía el porcentaje de velocidad a la comutadora  conectada remotamente*/
	int porcentEnviar = (int) porcentaje;			
	sprintf(command, "echo P%c > /dev/ttyS0", porcentEnviar+48); // command es un arreglo que recibe el segundo parámetro y queda un string
	while(serialGetchar(fd) != 122)	// Mientras la computadora no indique que le llegó el porcentaje se sigue enviando 
		system(command);		// La computadora envía 'z' cuando llega el porcentaje
						// Para diferencia el porcentaje de otra información, se envía una 'P' y luego el porcentaje
	do{
		if(((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100) || aux1 == 0){	
			system("echo \"\e[47m\"");
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format9, format10);
			printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 Auto Fantástico \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s      %s                                                                 \n", format9, format12);
		}
		aux1 = 1;

		if(serialDataAvail(fd)){
			input = serialGetchar(fd);		
		}

		inputRasp = getChar4();	// Se llama a la actualización de la función getchar()

		if(input == 'A')
			porcentaje +=  5;
		if(input == 'B')
			porcentaje -= 5;

		if(porcentaje > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;

		tDelay = porcentaje * (tiempo_min - tiempo_max) / 100 + tiempo_max; // Se actualiza tDelay en caso de que se haya modificado el porcentaje de velocidad

		if((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100){
			aux2 = tDelay;
			aux3 = 0;
		}
		
		contador--;
		if(contador == 0){
			if(aux4 == 0){
				Auto_Fantastico(1);
				aux4 = 1;
			}else{
				Auto_Fantastico(0);
			}	
			if(aux3 == 0)
				contador = aux2;
			else
				contador = tDelay;
		}
		
		delayMillis(1); 
	}while(input != 113 && inputRasp != 113 && inputRasp != 81);		
	
	system("setterm -curso on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);

	apagarLuces();
	
	if(inputRasp == 113){
		system("echo 'q' > /dev/ttyS0");
		delayMillis(20);	// Debido a alguna configuración en el puerto destino, cuando se envía un caracter, el destino devuelve nuevamente el caracter
		serialFlush(fd);	// esto hace que al llegar al puerto de la raspberry quede esperando a ser leído y cuando se vuelve a entrar al menúPrincipalRemoto
	}				// entra y sale instantaneamente e indeseadamente
}

void elChoqueRemoto(){
	float porcentaje = porcentajes[1];	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	int tDelay = tDelays[1]; 		// Se carga el tiempo con el valor inicializado con el potenciómetro
	char input = 0;
	char inputRasp = 0;
	int aux1 = 0;				// Arranca en 0 para mostrar la pantalla la primera vez, cuando termina la primera pasada
						// se pone en 1 para que solo se refresque cuando se oprime una tecla
	int aux2 = 0;				// Sirve para que al cambiar de velocidad, el cambio ocurra en la próxima "prendida" o "apagad" de las luces
	int aux3 = 1;				// Indica si el contador debe setearse con el valor inicial o que proviene como resultado de modoficar la velocidad
	int contador = tDelay;
	char command[24];
	int aux4 = 0;

	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");
	
	apagarLuces();

	int porcentEnviar = (int) porcentaje;
	sprintf(command, "echo P%c > /dev/ttyS0", porcentEnviar+48);
	while(serialGetchar(fd) != 122)
		system(command);
	
	do{
		
		if(((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100) || aux1 == 0){	
			system("echo \"\e[47m\"");
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format9, format10);
			printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 El  Choque \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s      %s                                                                 \n", format9, format12);
		}
		aux1 = 1;

		if(serialDataAvail(fd)){
			input = serialGetchar(fd);		
		}

		inputRasp = getChar4();	// Se llama a la actualización de la función getchar()

		if(input == 'A')
			porcentaje +=  5;
		if(input == 'B')
			porcentaje -= 5;

		if(porcentaje > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;

		tDelay = porcentaje * (tiempo_min - tiempo_max) / 100 + tiempo_max; // Se actualiza tDelay en caso de que se haya modificado el porcentaje de velocidad

		if((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100){
			aux2 = tDelay;
			aux3 = 0;
		}
		
		contador--;
		if(contador == 0){
			if(aux4 == 0){
				El_Choque(1);
				aux4 = 1;
			}else{
				El_Choque(0);
			}	
			if(aux3 == 0)
				contador = aux2;
			else
				contador = tDelay;
		}
		
		delayMillis(1);
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);		
	
	system("setterm -curso on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);

	apagarLuces();

	if(inputRasp == 113){
		system("echo 'q' > /dev/ttyS0");
		delayMillis(20);	// Debido a alguna configuración en el puerto destino, cuando se envía un caracter, el destino devuelve nuevamente el caracter
		serialFlush(fd);	// esto hace que al llegar al puerto de la raspberry quede esperando a ser leído y cuando se vuelve a entrar al menúPrincipalRemoto
	}	
}

void laApiladaRemoto(){
	float porcentaje = porcentajes[2];	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	int tDelay = tDelays[2]; 		// Se carga el tiempo con el valor inicializado con el potenciómetro
	char input = 0;
	char inputRasp = 0;
	int aux1 = 0;				// Arranca en 0 para mostrar la pantalla la primera vez, cuando termina la primera pasada
						// se pone en 1 para que solo se refresque cuando se oprime una tecla
	int aux2 = 0;				// Sirve para que al cambiar de velocidad, el cambio ocurra en la próxima "prendida" o "apagad" de las luces
	int aux3 = 1;				// Indica si el contador debe setearse con el valor inicial o que proviene como resultado de modoficar la velocidad
	int contador = tDelay;
	char command[24];
	int aux4 = 0;

	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");

	apagarLuces();

	int porcentEnviar = (int) porcentaje;
	sprintf(command, "echo P%c > /dev/ttyS0", porcentEnviar+48);
	while(serialGetchar(fd) != 122)
		system(command);

	do{
		
		if(((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100) || aux1 == 0){	
			system("echo \"\e[47m\"");
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format9, format10);
			printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 La Apilada \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s      %s                                                                 \n", format9, format12);
		}
		aux1 = 1;

		if(serialDataAvail(fd)){
			input = serialGetchar(fd);	
		}

		inputRasp = getChar4();	// Se llama a la actualización de la función getchar()

		if(input == 'A')
			porcentaje +=  5;
		if(input == 'B')
			porcentaje -= 5;

		if(porcentaje > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;

		tDelay = porcentaje * (tiempo_min - tiempo_max) / 100 + tiempo_max; // Se actualiza tDelay en caso de que se haya modificado el porcentaje de velocidad

		if((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100){
			aux2 = tDelay;
			aux3 = 0;
		}
		
		contador--;
		if(contador == 0){
			if(aux4 == 0){
				La_Apilada(1);
				aux4 = 1;
			}else{
				La_Apilada(0);
			}	
			if(aux3 == 0)
				contador = aux2;
			else
				contador = tDelay;
		}
		
		delayMillis(1);
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);		
	
	system("setterm -curso on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);

	serialFlush(fd);

	apagarLuces();

	if(inputRasp == 113){
		system("echo 'q' > /dev/ttyS0");
		delayMillis(20);	// Debido a alguna configuración en el puerto destino, cuando se envía un caracter, el destino devuelve nuevamente el caracter
		serialFlush(fd);	// esto hace que al llegar al puerto de la raspberry quede esperando a ser leído y cuando se vuelve a entrar al menúPrincipalRemoto
	}	
}

void laCarreraRemoto(){
	float porcentaje = porcentajes[3];	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	int tDelay = tDelays[3]; 		// Se carga el tiempo con el valor inicializado con el potenciómetro
	char input = 0;
	char inputRasp = 0;
	int aux1 = 0;				// Arranca en 0 para mostrar la pantalla la primera vez, cuando termina la primera pasada
						// se pone en 1 para que solo se refresque cuando se oprime una tecla
	int aux2 = 0;				// Sirve para que al cambiar de velocidad, el cambio ocurra en la próxima "prendida" o "apagad" de las luces
	int aux3 = 1;				// Indica si el contador debe setearse con el valor inicial o que proviene como resultado de modoficar la velocidad
	int contador = tDelay;
	char command[24];
	int aux4 = 0;

	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");

	apagarLuces();

	int porcentEnviar = (int) porcentaje;
	sprintf(command, "echo P%c > /dev/ttyS0", porcentEnviar+48);
	while(serialGetchar(fd) != 122)
		system(command);
	
	do{
		
		if(((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100) || aux1 == 0){	
			system("echo \"\e[47m\"");
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format9, format10);
			printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 La Carrera \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s      %s                                                                 \n", format9, format12);
		}
		aux1 = 1;

		if(serialDataAvail(fd)){
			input = serialGetchar(fd);		
		}

		inputRasp = getChar4();	// Se llama a la actualización de la función getchar()

		if(input == 'A')
			porcentaje +=  5;
		if(input == 'B')
			porcentaje -= 5;

		if(porcentaje > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;

		tDelay = porcentaje * (tiempo_min - tiempo_max) / 100 + tiempo_max; // Se actualiza tDelay en caso de que se haya modificado el porcentaje de velocidad

		if((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100){
			aux2 = tDelay;
			aux3 = 0;
		}
		
		contador--;
		if(contador == 0){
			if(aux4 == 0){
				La_Carrera(1);
				aux4 = 1;
			}else{
				La_Carrera(0);
			}	
			if(aux3 == 0)
				contador = aux2;
			else
				contador = tDelay;
		}
		
		delayMillis(1);
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);		
	
	system("setterm -curso on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);

	apagarLuces();

	if(inputRasp == 113){
		system("echo 'q' > /dev/ttyS0");
		delayMillis(20);	// Debido a alguna configuración en el puerto destino, cuando se envía un caracter, el destino devuelve nuevamente el caracter
		serialFlush(fd);	// esto hace que al llegar al puerto de la raspberry quede esperando a ser leído y cuando se vuelve a entrar al menúPrincipalRemoto
	}
}

void contadorBinarioRemoto(){
	float porcentaje = porcentajes[4];	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	int tDelay = tDelays[4]; 		// Se carga el tiempo con el valor inicializado con el potenciómetro
	char input = 0;
	char inputRasp = 0;
	int aux1 = 0;				// Arranca en 0 para mostrar la pantalla la primera vez, cuando termina la primera pasada
						// se pone en 1 para que solo se refresque cuando se oprime una tecla
	int aux2 = 0;				// Sirve para que al cambiar de velocidad, el cambio ocurra en la próxima "prendida" o "apagad" de las luces
	int aux3 = 1;				// Indica si el contador debe setearse con el valor inicial o que proviene como resultado de modoficar la velocidad
	int contador = tDelay;
	char command[24];
	int aux4 = 0;

	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");

	apagarLuces();

	int porcentEnviar = (int) porcentaje;
	sprintf(command, "echo P%c > /dev/ttyS0", porcentEnviar+48);
	while(serialGetchar(fd) != 122)
		system(command);
	
	do{
		
		if(((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100) || aux1 == 0){	
			system("echo \"\e[47m\"");
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format9, format10);
			printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 Contador Binario \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s      %s                                                                 \n", format9, format12);
		}
		aux1 = 1;

		if(serialDataAvail(fd)){
			input = serialGetchar(fd);		
		}


		inputRasp = getChar4();	// Se llama a la actualización de la función getchar()

		if(input == 'A')
			porcentaje +=  5;
		if(input == 'B')
			porcentaje -= 5;

		if(porcentaje > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;

		tDelay = porcentaje * (tiempo_min - tiempo_max) / 100 + tiempo_max; // Se actualiza tDelay en caso de que se haya modificado el porcentaje de velocidad

		if((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100){
			aux2 = tDelay;
			aux3 = 0;
		}
		
		contador--;
		if(contador == 0){
			if(aux4 == 0){
				Binario(1);
				aux4 = 1;
			}else{
				Binario(0);
			}	
			if(aux3 == 0)
				contador = aux2;
			else
				contador = tDelay;
		}
		
		delayMillis(1);
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);		
	
	system("setterm -curso on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);

	apagarLuces();

	if(inputRasp == 113){
		system("echo 'q' > /dev/ttyS0");
		delayMillis(20);	// Debido a alguna configuración en el puerto destino, cuando se envía un caracter, el destino devuelve nuevamente el caracter
		serialFlush(fd);	// esto hace que al llegar al puerto de la raspberry quede esperando a ser leído y cuando se vuelve a entrar al menúPrincipalRemoto
	}
}

void penduloNewtonRemoto(){
	float porcentaje = porcentajes[5];	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	int tDelay = tDelays[5]; 		// Se carga el tiempo con el valor inicializado con el potenciómetro
	char input = 0;
	char inputRasp = 0;
	int aux1 = 0;				// Arranca en 0 para mostrar la pantalla la primera vez, cuando termina la primera pasada
						// se pone en 1 para que solo se refresque cuando se oprime una tecla
	int aux2 = 0;				// Sirve para que al cambiar de velocidad, el cambio ocurra en la próxima "prendida" o "apagad" de las luces
	int aux3 = 1;				// Indica si el contador debe setearse con el valor inicial o que proviene como resultado de modoficar la velocidad
	int contador = tDelay;
	char command[24];
	int aux4 = 0;

	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");

	apagarLuces();

	int porcentEnviar = (int) porcentaje;
	sprintf(command, "echo P%c > /dev/ttyS0", porcentEnviar+48);
	while(serialGetchar(fd) != 122)
		system(command);

	do{
		
		if(((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100) || aux1 == 0){	
			system("echo \"\e[47m\"");
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format9, format10);
			printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 Péndulo de Newton \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s      %s                                                                 \n", format9, format12);
		}
		aux1 = 1;

		if(serialDataAvail(fd)){
			input = serialGetchar(fd);		
		}

		inputRasp = getChar4();	// Se llama a la actualización de la función getchar()

		if(input == 'A')
			porcentaje +=  5;
		if(input == 'B')
			porcentaje -= 5;

		if(porcentaje > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;

		tDelay = porcentaje * (tiempo_min - tiempo_max) / 100 + tiempo_max; // Se actualiza tDelay en caso de que se haya modificado el porcentaje de velocidad

		if((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100){
			aux2 = tDelay;
			aux3 = 0;
		}
		
		contador--;
		if(contador == 0){
			if(aux4 == 0){
				Pendulo_Newton(1);
				aux4 = 1;
			}else{
				Pendulo_Newton(0);
			}
			if(aux3 == 0)
				contador = aux2;
			else
				contador = tDelay;
		}
		
		delayMillis(1);
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);		
	
	system("setterm -curso on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);

	apagarLuces();

	if(inputRasp == 113){
		system("echo 'q' > /dev/ttyS0");
		delayMillis(20);	// Debido a alguna configuración en el puerto destino, cuando se envía un caracter, el destino devuelve nuevamente el caracter
		serialFlush(fd);	// esto hace que al llegar al puerto de la raspberry quede esperando a ser leído y cuando se vuelve a entrar al menúPrincipalRemoto
	}
}

void alternadoRemoto(){
	float porcentaje = porcentajes[6];	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	int tDelay = tDelays[6]; 		// Se carga el tiempo con el valor inicializado con el potenciómetro
	char input = 0;
	char inputRasp = 0;
	int aux1 = 0;				// Arranca en 0 para mostrar la pantalla la primera vez, cuando termina la primera pasada
						// se pone en 1 para que solo se refresque cuando se oprime una tecla
	int aux2 = 0;				// Sirve para que al cambiar de velocidad, el cambio ocurra en la próxima "prendida" o "apagad" de las luces
	int aux3 = 1;				// Indica si el contador debe setearse con el valor inicial o que proviene como resultado de modoficar la velocidad
	int contador = tDelay;
	char command[24];
	int aux4 = 0;

	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");

	apagarLuces();

	int porcentEnviar = (int) porcentaje;
	sprintf(command, "echo P%c > /dev/ttyS0", porcentEnviar+48);
	while(serialGetchar(fd) != 122)
		system(command);

	do{
		
		if(((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100) || aux1 == 0){	
			system("echo \"\e[47m\"");
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format9, format10);
			printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 Alternado \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s      %s                                                                 \n", format9, format12);
		}
		aux1 = 1;

		if(serialDataAvail(fd)){
			input = serialGetchar(fd);		
		}

		inputRasp = getChar4();	// Se llama a la actualización de la función getchar()

		if(input == 'A')
			porcentaje +=  5;
		if(input == 'B')
			porcentaje -= 5;

		if(porcentaje > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;

		tDelay = porcentaje * (tiempo_min - tiempo_max) / 100 + tiempo_max; // Se actualiza tDelay en caso de que se haya modificado el porcentaje de velocidad

		if((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100){
			aux2 = tDelay;
			aux3 = 0;
		}
		
		contador--;
		if(contador == 0){
			if(aux4 == 0){
				Alterno(1);
				aux4 = 1;
			}else{
				Alterno(0);
			}	
			if(aux3 == 0)
				contador = aux2;
			else
				contador = tDelay;
		}
		
		delayMillis(1);
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);		
	
	system("setterm -curso on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);

	apagarLuces();

	if(inputRasp == 113){
		system("echo 'q' > /dev/ttyS0");
		delayMillis(20);	// Debido a alguna configuración en el puerto destino, cuando se envía un caracter, el destino devuelve nuevamente el caracter
		serialFlush(fd);	// esto hace que al llegar al puerto de la raspberry quede esperando a ser leído y cuando se vuelve a entrar al menúPrincipalRemoto
	}
}

void reboteRemoto(){
	float porcentaje = porcentajes[7];	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	int tDelay = tDelays[7]; 		// Se carga el tiempo con el valor inicializado con el potenciómetro
	char input = 0;
	char inputRasp = 0;
	int aux1 = 0;				// Arranca en 0 para mostrar la pantalla la primera vez, cuando termina la primera pasada
						// se pone en 1 para que solo se refresque cuando se oprime una tecla
	int aux2 = 0;				// Sirve para que al cambiar de velocidad, el cambio ocurra en la próxima "prendida" o "apagad" de las luces
	int aux3 = 1;				// Indica si el contador debe setearse con el valor inicial o que proviene como resultado de modoficar la velocidad
	int contador = tDelay;
	char command[23];
	int aux4 = 0;

	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");

	apagarLuces();

	int porcentEnviar = (int) porcentaje;
	sprintf(command, "echo P%c > /dev/ttyS0", porcentEnviar+48);
	while(serialGetchar(fd) != 122)
		system(command);

	do{
		
		if(((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100) || aux1 == 0){	
			system("echo \"\e[47m\"");
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format9, format10);
			printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 Rebote \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format3);
			printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s      %s                                                                 \n", format9, format12);
		}
		aux1 = 1;

		if(serialDataAvail(fd)){
			input = serialGetchar(fd);		
		}

		inputRasp = getChar4();	// Se llama a la actualización de la función getchar()

		if(input == 'A')
			porcentaje +=  5;
		if(input == 'B')
			porcentaje -= 5;

		if(porcentaje > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;

		tDelay = porcentaje * (tiempo_min - tiempo_max) / 100 + tiempo_max; // Se actualiza tDelay en caso de que se haya modificado el porcentaje de velocidad

		if((input == 'A' || input == 'B') && porcentaje >= 0 && porcentaje <= 100){
			aux2 = tDelay;
			aux3 = 0;
		}
		
		contador--;
		if(contador == 0){
			if(aux4 == 0){
				Rebote(1);
				aux4 = 1;
			}else{
				Rebote(0);
			}
			if(aux3 == 0)
				contador = aux2;
			else
				contador = tDelay;
		}
		
		delayMillis(1);
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);		
	
	system("setterm -curso on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);

	apagarLuces();

	if(inputRasp == 113){
		system("echo 'q' > /dev/ttyS0");
		delayMillis(20);	// Debido a alguna configuración en el puerto destino, cuando se envía un caracter, el destino devuelve nuevamente el caracter
		serialFlush(fd);	// esto hace que al llegar al puerto de la raspberry quede esperando a ser leído y cuando se vuelve a entrar al menúPrincipalRemoto
	}
}

