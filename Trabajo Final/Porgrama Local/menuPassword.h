#include <stdlib.h>
#include <stdio.h>
#include <termios.h>
#include <errno.h>
#include <string.h>
#include "EasyPIO.h"

#define FD_STDIN 0
#define format0 "\e[0;30;47m"		// Fondo
#define format1 "\e[0;97;100m"		// Cuadro
#define format2 "\e[1;5;32;100m"	// Título Bienvenido
#define format3 "\e[0;30;40m"		// Sombra
#define format4 "\e[0;1;91;100m"	// Contraseña Incorrecta

#define N 16 // Permite una contraseña de 15 caracteres de largo

int menuPassword(){
	char pass[] = "password";	
	char insertedPass[N] = {0};		
	char character;		
	int intento = 3;	// Número de intentos
	int i, j;
	int length = 0;
	int acceso = 1; // Se inicializa en 1, Acceso Denegado por seguridad
	char asteriscos[N] = {0}; 

	struct termios t_old, t_new;	
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	tcsetattr(FD_STDIN, TCSANOW, &t_new);	
	system("setterm -cursor off");
	
	pioInit();

	system("clear");

	do{
		i = 0;
		for(i; i < N; i++)		// Se formatea el arreglo
			insertedPass[i] = 0;
		i = 0; 
		character = 0;
		
		system("echo \"e[0m\"");
		system("clear");
		system("echo \"\e[47m\"");
		system("clear");
		printf("\n\n\n\n\n\n\n\n\n\n\n\n");
		printf("%s    %s                                                                 \n", format0, format1);
		printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 LOG  IN \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format0, format1, format3);
		printf("%s    %s                                                                 %s  \n", format0, format1, format3);
		printf("%s    %s          Ingrese su Contraseña:                                 %s  \n", format0, format1, format3);
		printf("%s    %s                                                                 %s  \n", format0, format1, format3);
		printf("%s    %s                                                                 %s  \n", format0, format1, format3);
		printf("%s    %s                                                                 %s  \n", format0, format1, format3);
		printf("%s    %s                                                                 %s  \n", format0, format1, format3);
		printf("%s    %s                                                                 %s  \n", format0, format1, format3);
		printf("%s    %s                                                                 %s  \n", format0, format1, format3);
		printf("%s    %s                                                                 %s  \n", format0, format1, format3);
		printf("%s    %s                                                                 %s  \n", format0, format1, format3);
		printf("%s      %s                                                                 \n", format0, format3);
	
		while(character != 10){		
			character = getchar();
			if(character != 10 && character != 127 && i <= N-2){ 
				insertedPass[i] = character; // Caracter ingresado se pone en el array
				
				asteriscos[length] = 0;	
				strcat(asteriscos, "*");
				length++;
				for(int k = length; k < N-1; k++)
					asteriscos[k] = ' ';
				asteriscos[N-1] = 0;

				system("echo \"e[0m\"");
				system("clear");
				system("echo \"\e[47m\"");
				system("clear");
				printf("\n\n\n\n\n\n\n\n\n\n\n\n");
				printf("%s    %s                                                                 \n", format0, format1);
				printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 LOG  IN \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format0, format1, format3);
				printf("%s    %s                                                                 %s  \n", format0, format1, format3);
				printf("%s    %s          Ingrese su Contraseña: %s                 %s  \n", format0, format1, asteriscos, format3);
				printf("%s    %s                                                                 %s  \n", format0, format1, format3);
				printf("%s    %s                                                                 %s  \n", format0, format1, format3);
				printf("%s    %s                                                                 %s  \n", format0, format1, format3);
				printf("%s    %s                                                                 %s  \n", format0, format1, format3);
				printf("%s    %s                                                                 %s  \n", format0, format1, format3);
				printf("%s    %s                                                                 %s  \n", format0, format1, format3);
				printf("%s    %s                                                                 %s  \n", format0, format1, format3);
				printf("%s    %s                                                                 %s  \n", format0, format1, format3);
				printf("%s      %s                                                                 \n", format0, format3);
			}
			if(character == 127){
				j = i;
				for(j; j < N; j++)
					insertedPass[j] = 0;
				if(i > 0){
					i--;
					insertedPass[i] = 0;
				
					length--;
					for(int k = length; k < N-1; k++)
						asteriscos[k] = ' ';
					asteriscos[N-1] = 0;
				
					system("echo \"e[0m\"");
					system("clear");
					system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format0, format1);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 LOG  IN \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format0, format1, format3);
					printf("%s    %s                                                                 %s  \n", format0, format1, format3);
					printf("%s    %s          Ingrese su Contraseña: %s                 %s  \n", format0, format1, asteriscos, format3);
					printf("%s    %s                                                                 %s  \n", format0, format1, format3);
					printf("%s    %s                                                                 %s  \n", format0, format1, format3);
					printf("%s    %s                                                                 %s  \n", format0, format1, format3);
					printf("%s    %s                                                                 %s  \n", format0, format1, format3);
					printf("%s    %s                                                                 %s  \n", format0, format1, format3);
					printf("%s    %s                                                                 %s  \n", format0, format1, format3);
					printf("%s    %s                                                                 %s  \n", format0, format1, format3);
					printf("%s    %s                                                                 %s  \n", format0, format1, format3);
					printf("%s      %s                                                                 \n", format0, format3);
				}
				if(i < 0){	
					i = 0;
					asteriscos[0] = 0;
				}
			}else{
				if(i <= N-2)
					i++;
			}
		}	
		insertedPass[i] = 0;

		intento--;
		if(!(strcmp(pass, insertedPass))){
			intento  = 0;
			acceso = 1;
			system("echo \"\e[47m\"");
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format0, format1);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                     ¡BIENVENIDO AL SISTEMA!                     %s  \n", format0, format2, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s      %s                                                                 \n", format0, format3);
			delayMillis(4000); 
			system("echo \"\e[47m\"");
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format0, format1);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s        \u2022 Presione Enter [ \u23ce ] para Seleccionar                  %s  \n", format0, format1, format3);
			printf("%s    %s        \u2022 Presione \u2191 para Moverse hacia Arriba                   %s  \n", format0, format1, format3);
			printf("%s    %s        \u2022 Presione \u2193 para Moverse hacia Abajo                    %s  \n", format0, format1, format3);
			printf("%s    %s        \u2022 Presione \u2192 para Moverse hacia la Derecha               %s  \n", format0, format1, format3);
			printf("%s    %s        \u2022 Presione \u2190 para Moverse hacia la Izquierda             %s  \n", format0, format1, format3);
			printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s      %s                                                                 \n", format0, format3);
			delayMillis(10000);
		}else if(intento == 2){
			acceso = 0;
			asteriscos[0] = 0;
			length = 0;
			system("echo \"\e[47m\"");
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format0, format1);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                      CONTRASEÑA INCORRECTA                      %s  \n", format0, format4, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s     Quedan %d intentos.                                          %s  \n", format0, format4, intento, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3); 
			printf("%s      %s                                                                 \n", format0, format3);
			delayMillis(4000); 			
		}else if(intento == 1){
			acceso = 0;
			asteriscos[0] = 0;
			length = 0;
			system("echo \"\e[47m\"");
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format0, format1);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                      CONTRASEÑA INCORRECTA                      %s  \n", format0, format4, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s     Queda %d intento.                                            %s  \n", format0, format4, intento, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3); 
			printf("%s      %s                                                                 \n", format0, format3);
			delayMillis(4000); 
		}else if(intento == 0){
			acceso = 0;
			asteriscos[0] = 0;
			length = 0;
			system("echo \"\e[47m\"");
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format0, format1);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                        ¡ACCESO DENEGADO!                        %s  \n\a", format0, format4, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3);
			printf("%s    %s                                                                 %s  \n", format0, format4, format3);
			printf("%s    %s                                                                 %s  \n", format0, format1, format3); 
			printf("%s      %s                                                                 \n", format0, format3);
			delayMillis(4000); 	
			system("echo \"\e[0m\"");		
			system("clear");
		}		
	}while(intento != 0);

	tcsetattr(FD_STDIN, TCSANOW, &t_old);
	system("setterm -cursor on");
	return acceso;
}
