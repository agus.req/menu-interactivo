#include "EasyPIO.h"

#define led0 23
#define led1 24
#define led2 25
#define led3 12
#define led4 16
#define led5 20
#define led6 21
#define led7 26

#define push1 17
#define push2 27
#define dipSw1 22
#define dipSw2 5
#define dipSw3 6
#define dipSw4 13
#define dipSw5 19

void GPIOConfig(void){		// Configuración de la GPIO para la placa utilizada en este proyecto
	pioInit();
	
	pinMode(led0, OUTPUT);
	pinMode(led1, OUTPUT);
	pinMode(led2, OUTPUT);
	pinMode(led3, OUTPUT);
	pinMode(led4, OUTPUT);
	pinMode(led5, OUTPUT);
	pinMode(led6, OUTPUT);
	pinMode(led7, OUTPUT);
	pinMode(push1,INPUT);
	pinMode(push2,INPUT);
	pinMode(dipSw1,INPUT);
	pinMode(dipSw2,INPUT);
	pinMode(dipSw3,INPUT);
	pinMode(dipSw4,INPUT);
	pinMode(dipSw5,INPUT);
}
