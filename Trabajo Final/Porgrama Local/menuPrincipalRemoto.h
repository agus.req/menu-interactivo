#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <errno.h>
#include "EasyPIO.h"
#include <wiringSerial.h>
#include <wiringPi.h>

#define FD_STDIN 0
#define format9 "\e[30;47m"	// Fondo
#define format10 "\e[97;100m"	// Cuadro
#define format11 "\e[91;107m"	// Seleccionado
#define format12 "\e[30;40m"	// Sombra

extern int fd;

char getChar3(){			// Función actualizada de getchar para que no sea bloqueante
	char buff[2];		// No alcanza con solo modificar la configuración de termios
	int l = read(STDIN_FILENO, buff, 1);	// porque solo con eso no toma caracteres del buffer de entrada
	if(l>0)					// gits.github.com/whyrusleeping/3983293
		return buff[0];
	return(EOF);
}

int menuPrincipalRemoto(){
	int i;
	int secuencia = 0;
	char input = 0;
	char inputRasp = 0;

	struct termios t_old, t_new;	
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");

	serialFlush(fd);
	
	system("echo \"\e[47m\"");
	system("clear");
	printf("\n\n\n\n\n\n\n\n\n\n\n\n");
	printf("%s    %s                                                                 \n", format9, format10);
	printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
	printf("%s    %s                                                                 %s  \n", format9, format10, format12);
	printf("%s    %s         %s 1) Auto Fantástico %s                                    %s  \n", format9, format10, format11, format1, format12);
	printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
	printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
	printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
	printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
	printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
	printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
	printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
	printf("%s    %s                                                                 %s  \n", format9, format10, format12);
	printf("%s      %s                                                                 \n", format9, format12);
	
	do{
		if(serialDataAvail(fd)){
			input = serialGetchar(fd);		
		}

		inputRasp = getChar3();

		if(input == 'A')
			secuencia--;
		if(input == 'B')
			secuencia++;

		if(secuencia > 7)
			secuencia = 7;
		if(secuencia < 0)
			secuencia = 0;
			
		switch(secuencia){
			case 0:		
					system("echo \"\e[47m\"");		
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s         %s 1) Auto Fantástico %s                                    %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);							
					break;
			case 1:		
					system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s         %s 2) El Choque %s                                          %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 2:		
					system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10,format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s         %s 3) La Apilada %s                                         %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 3:		
					system("echo \"\e[47m\"");		
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s         %s 4) La Carrera %s                                         %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 4:		
					system("echo \"\e[47m\"");		
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s         %s 5) Contador Binario %s                                   %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);							
					break;
			case 5:	
					system("echo \"\e[47m\"");	
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10,format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s         %s 6) Péndulo de Newton %s                                  %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 6:		
					system("echo \"\e[47m\"");	
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s         %s 7) Alternado %s                                          %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 7:		
					system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s         %s 8) Rebote %s                                             %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;		
		}

		delayMillis(100);
	}while(input != 113 && input != 81 && input != 115 && inputRasp != 113 && inputRasp != 81);		
	// 115 reemplaza al Enter que no puede utilizarse debido a que el puerto serie recibe un Enter cada vez		
	// que se envía un comando desde la computadora conectada por RS-232
	
	/* Se reestablece la configuración de la Terminal */
	system("setterm -curso on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);

	if(inputRasp == 113 || inputRasp == 81){
		system("echo 'q' > /dev/ttyS0");
		delayMillis(20);	// Debido a alguna configuración en el puerto destino, cuando se envía un caracter, el destino devuelve nuevamente el caracter
		serialFlush(fd);	// esto hace que al llegar al puerto de la raspberry quede esperando a ser leído y cuando se vuelve a entrar al menúPrincipalRemoto
					// entra y sale instantaneamente e indeseadamente
		return 113;
	}	
	if(input == 113 || input == 81)
		return 113;

	if(input == 115)
		return secuencia;
}
