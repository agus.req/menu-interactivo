#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <errno.h>
#include "EasyPIO.h"
#include "adc.h"

#define FD_STDIN 0
#define format9 "\e[30;47m"	// Fondo
#define format10 "\e[97;100m"	// Cuadro
#define format11 "\e[91;107m"	// Seleccionado
#define format12 "\e[30;40m"	// Sombra


extern int leyendaConfigVel;
extern float tiempo_min;	// Tiempo mínimo en la secuencia de luces, extern para que sea global a todo el proyecto
extern float tiempo_max;	// Tiempo máximo en la secuencia de luces, extern para que sea global a todo el proyecto
extern int tDelays [8];		// Tiempo de cada una de las secuencias de luces
extern float porcentajes[8];	// Porcentajes de velocidad de cada una de las secuencias de luces

char getChar2(){		// Función actualizada de getchar para que no sea bloqueante
	char buff[2];		// No alcanza con solo modificar la configuración de termios
	int l = read(STDIN_FILENO, buff, 1);	// porque solo con eso no toma caracteres del buffer de entrada
	if(l>0)					// gits.github.com/whyrusleeping/3983293
		return buff[0];
	return(EOF);
}

int menuConfigVelocidades(){
	int lectura = 0;
	int i;
	int secuencia = 0;
	char input;
	float porcentaje = 0;
	int tDelay;

	struct termios t_old, t_new;	
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");

	lectura = adc();		// Se lee el ADC, entrega un valor comprendido entre 0 y 255
	porcentaje = (int) (0.392 * lectura); 	// Porcentaje, valor de 0 corresponde al 0% y valor de 255 corresponde al 100% 
	tDelay = porcentaje * (tiempo_min - tiempo_max) / 100 + tiempo_max;	// Teniendo en cuenta los tiempos mínimos y máximos 
									   	// y el porcentaje seleccionado mediante el potenciómetro
										// se calcula el tiempo de delay de las secuencias	
	if(leyendaConfigVel == 0){	
		system("echo \"\e[47m\"");
		system("clear");
		printf("\n\n\n\n\n\n\n\n\n\n\n\n");
		printf("%s    %s                                                                 \n", format9, format10);
		printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 VELOCIDADES INICIALES \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s         Aquí se setean las velocidades inciales de cada         %s  \n", format9, format10, format12);
		printf("%s    %s         secuencia de luces.                                     %s  \n", format9, format10, format12);
		printf("%s    %s         El valor de la velocidad inicial se ajusta con          %s  \n", format9, format10, format12);
		printf("%s    %s         el potenciómetro de la placa externa.                   %s  \n", format9, format10, format12);
		printf("%s    %s         Desplácese por el menú siguiente y oprima Enter         %s  \n", format9, format10, format12);
		printf("%s    %s         [ \u23CE ] para setear la velocidad.                         %s  \n", format9, format10, format12);
		printf("%s    %s         La velocidad 0%c corresponde a un tiempo de              %s  \n", format9, format10, '%', format12);
	       	printf("%s    %s         %.2f segundos y 100%c a %.2f milisegundos.              %s  \n", format9, format10, tiempo_max/1000, '%', tiempo_min, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12); 
		printf("%s      %s                                                                 \n", format9, format12);
		leyendaConfigVel = 1;
		delayMillis(20000);
	}

	system("echo \"\e[47m\"");		
	system("clear");
	printf("\n\n\n\n\n\n\n\n\n\n\n\n");
	printf("%s    %s                                                                 \n", format9, format10);
	printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 VELOCIDADES INICIALES \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);	
	printf("%s    %s                                                                 %s  \n", format9, format10, format12);
	printf("%s    %s         %s 1) Auto Fantástico %s      %3.0f%c                          %s  \n", format9, format10, format11, format1, porcentajes[0], '%',format12);	
	printf("%s    %s          2) El Choque             %3.0f%c                          %s  \n", format9, format10, porcentajes[1], '%', format12);
	printf("%s    %s          3) La Apilada            %3.0f%c                          %s  \n", format9, format10, porcentajes[2], '%', format12);	
	printf("%s    %s          4) La Carrera            %3.0f%c                          %s  \n", format9, format10, porcentajes[3], '%', format12);
	printf("%s    %s          5) Contador Binario      %3.0f%c                          %s  \n", format9, format10, porcentajes[4], '%', format12);
	printf("%s    %s          6) Péndulo de Newton     %3.0f%c                          %s  \n", format9, format10, porcentajes[5], '%', format12);
	printf("%s    %s          7) Alternado             %3.0f%c                          %s  \n", format9, format10, porcentajes[6], '%', format12);
	printf("%s    %s          8) Rebote                %3.0f%c                          %s  \n", format9, format10, porcentajes[7], '%', format12);
	printf("%s    %s                                                                 %s  \n", format9, format10, format12);
	printf("%s      %s                                                                 \n", format9, format12);		

	do{	
		lectura = adc();		// Se lee el ADC, entrega un valor comprendido entre 0 y 255
		porcentaje = (int) (0.392 * lectura); 	// Porcentaje, valor de 0 corresponde al 0% y valor de 255 corresponde al 100% 
		tDelay = porcentaje * (tiempo_min - tiempo_max) / 100 + tiempo_max;	// Teniendo en cuenta los tiempos mínimos y máximos 
									   	// y el porcentaje seleccionado mediante el potenciómetro
										// se calcula el tiempo de delay de las secuencias

		input = getChar2();

		if(input == 'A')
			secuencia--;
		if(input == 'B')
			secuencia++;

		if(secuencia > 7)
			secuencia = 7;
		if(secuencia < 0)
			secuencia = 0;
			
		switch(secuencia){
			case 0:		system("echo \"\e[47m\"");		
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 VELOCIDADES INICIALES \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);	
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s         %s 1) Auto Fantástico %s      %3.0f%c                          %s  \n", format9, format10, format11, format1, porcentajes[0], '%',format12);
					printf("%s    %s          2) El Choque             %3.0f%c                          %s  \n", format9, format10, porcentajes[1], '%', format12);
					printf("%s    %s          3) La Apilada            %3.0f%c                          %s  \n", format9, format10, porcentajes[2], '%', format12);
					printf("%s    %s          4) La Carrera            %3.0f%c                          %s  \n", format9, format10, porcentajes[3], '%', format12);
					printf("%s    %s          5) Contador Binario      %3.0f%c                          %s  \n", format9, format10, porcentajes[4], '%', format12);
					printf("%s    %s          6) Péndulo de Newton     %3.0f%c                          %s  \n", format9, format10, porcentajes[5], '%', format12);
					printf("%s    %s          7) Alternado             %3.0f%c                          %s  \n", format9, format10, porcentajes[6], '%', format12);
					printf("%s    %s          8) Rebote                %3.0f%c                          %s  \n", format9, format10, porcentajes[7], '%', format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);							
					break;
			case 1:		system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 VELOCIDADES INICIALES \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);	
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico       %3.0f%c                          %s  \n", format9, format10, porcentajes[0], '%',format12);
					printf("%s    %s         %s 2) El Choque %s            %3.0f%c                          %s  \n", format9, format10, format11, format1, porcentajes[1], '%',format12);
					printf("%s    %s          3) La Apilada            %3.0f%c                          %s  \n", format9, format10, porcentajes[2], '%',format12);
					printf("%s    %s          4) La Carrera            %3.0f%c                          %s  \n", format9, format10, porcentajes[3], '%',format12);
					printf("%s    %s          5) Contador Binario      %3.0f%c                          %s  \n", format9, format10, porcentajes[4], '%',format12);
					printf("%s    %s          6) Péndulo de Newton     %3.0f%c                          %s  \n", format9, format10, porcentajes[5], '%',format12);
					printf("%s    %s          7) Alternado             %3.0f%c                          %s  \n", format9, format10, porcentajes[6], '%',format12);
					printf("%s    %s          8) Rebote                %3.0f%c                          %s  \n", format9, format10, porcentajes[7], '%',format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 2:		system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 VELOCIDADES INICIALES \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);	
					
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico       %3.0f%c                          %s  \n", format9, format10, porcentajes[0], '%',format12);
					printf("%s    %s          2) El Choque             %3.0f%c                          %s  \n", format9, format10, porcentajes[1], '%',format12);
					printf("%s    %s         %s 3) La Apilada %s           %3.0f%c                          %s  \n", format9, format10, format11, format1, porcentajes[2], '%',format12);
					printf("%s    %s          4) La Carrera            %3.0f%c                          %s  \n", format9, format10, porcentajes[3], '%',format12);
					printf("%s    %s          5) Contador Binario      %3.0f%c                          %s  \n", format9, format10, porcentajes[4], '%',format12);
					printf("%s    %s          6) Péndulo de Newton     %3.0f%c                          %s  \n", format9, format10, porcentajes[5], '%',format12);
					printf("%s    %s          7) Alternado             %3.0f%c                          %s  \n", format9, format10, porcentajes[6], '%',format12);
					printf("%s    %s          8) Rebote                %3.0f%c                          %s  \n", format9, format10, porcentajes[7], '%',format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 3:		system("echo \"\e[47m\"");		
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 VELOCIDADES INICIALES \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);	
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico       %3.0f%c                          %s  \n", format9, format10, porcentajes[0], '%',format12);
					printf("%s    %s          2) El Choque             %3.0f%c                          %s  \n", format9, format10, porcentajes[1], '%', format12);
					printf("%s    %s          3) La Apilada            %3.0f%c                          %s  \n", format9, format10, porcentajes[2], '%', format12);
					printf("%s    %s         %s 4) La Carrera %s           %3.0f%c                          %s  \n", format9, format10, format11, format1, porcentajes[3], '%',format12);
					printf("%s    %s          5) Contador Binario      %3.0f%c                          %s  \n", format9, format10, porcentajes[4], '%', format12);
					printf("%s    %s          6) Péndulo de Newton     %3.0f%c                          %s  \n", format9, format10, porcentajes[5], '%', format12);
					printf("%s    %s          7) Alternado             %3.0f%c                          %s  \n", format9, format10, porcentajes[6], '%', format12);
					printf("%s    %s          8) Rebote                %3.0f%c                          %s  \n", format9, format10, porcentajes[7], '%', format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 4:		system("echo \"\e[47m\"");		
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 VELOCIDADES INICIALES \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);	
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico       %3.0f%c                          %s  \n", format9, format10, porcentajes[0], '%',format12);
					printf("%s    %s          2) El Choque             %3.0f%c                          %s  \n", format9, format10, porcentajes[1], '%', format12);		
					printf("%s    %s          3) La Apilada            %3.0f%c                          %s  \n", format9, format10, porcentajes[2], '%', format12);
					printf("%s    %s          4) La Carrera            %3.0f%c                          %s  \n", format9, format10, porcentajes[3], '%', format12);		
					printf("%s    %s         %s 5) Contador Binario %s     %3.0f%c                          %s  \n", format9, format10, format11, format1, porcentajes[4], '%',format12);
					printf("%s    %s          6) Péndulo de Newton     %3.0f%c                          %s  \n", format9, format10, porcentajes[5], '%', format12);
					printf("%s    %s          7) Alternado             %3.0f%c                          %s  \n", format9, format10, porcentajes[6], '%', format12);
					printf("%s    %s          8) Rebote                %3.0f%c                          %s  \n", format9, format10, porcentajes[7], '%', format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);							
					break;
			case 5:		system("echo \"\e[47m\"");	
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 VELOCIDADES INICIALES \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);	
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico       %3.0f%c                          %s  \n", format9, format10, porcentajes[0], '%',format12);
					printf("%s    %s          2) El Choque             %3.0f%c                          %s  \n", format9, format10, porcentajes[1], '%', format12);
					printf("%s    %s          3) La Apilada            %3.0f%c                          %s  \n", format9, format10, porcentajes[2], '%', format12);
					printf("%s    %s          4) La Carrera            %3.0f%c                          %s  \n", format9, format10, porcentajes[3], '%', format12);
					printf("%s    %s          5) Contador Binario      %3.0f%c                          %s  \n", format9, format10, porcentajes[4], '%', format12);
					printf("%s    %s         %s 6) Péndulo de Newton %s    %3.0f%c                          %s  \n", format9, format10, format11, format1, porcentajes[5], '%',format12);
					printf("%s    %s          7) Alternado             %3.0f%c                          %s  \n", format9, format10, porcentajes[6], '%', format12);
					printf("%s    %s          8) Rebote                %3.0f%c                          %s  \n", format9, format10, porcentajes[7], '%', format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 6:		system("echo \"\e[47m\"");	
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 VELOCIDADES INICIALES \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);	
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico       %3.0f%c                          %s  \n", format9, format10, porcentajes[0], '%',format12);
					printf("%s    %s          2) El Choque             %3.0f%c                          %s  \n", format9, format10, porcentajes[1], '%', format12);
					printf("%s    %s          3) La Apilada            %3.0f%c                          %s  \n", format9, format10, porcentajes[2], '%', format12);
					printf("%s    %s          4) La Carrera            %3.0f%c                          %s  \n", format9, format10, porcentajes[3], '%', format12);
					printf("%s    %s          5) Contador Binario      %3.0f%c                          %s  \n", format9, format10, porcentajes[4], '%', format12);
					printf("%s    %s          6) Péndulo de Newton     %3.0f%c                          %s  \n", format9, format10, porcentajes[5], '%', format12);
					printf("%s    %s         %s 7) Alternado %s            %3.0f%c                          %s  \n", format9, format10, format11, format1, porcentajes[6], '%',format12);
					printf("%s    %s          8) Rebote                %3.0f%c                          %s  \n", format9, format10, porcentajes[7], '%', format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 7:		system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 VELOCIDADES INICIALES \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);	
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico       %3.0f%c                          %s  \n", format9, format10, porcentajes[0], '%',format12);
					printf("%s    %s          2) El Choque             %3.0f%c                          %s  \n", format9, format10, porcentajes[1], '%', format12);
					printf("%s    %s          3) La Apilada            %3.0f%c                          %s  \n", format9, format10, porcentajes[2], '%', format12);
					printf("%s    %s          4) La Carrera            %3.0f%c                          %s  \n", format9, format10, porcentajes[3], '%', format12);
					printf("%s    %s          5) Contador Binario      %3.0f%c                          %s  \n", format9, format10, porcentajes[4], '%', format12);
					printf("%s    %s          6) Péndulo de Newton     %3.0f%c                          %s  \n", format9, format10, porcentajes[5], '%', format12);
					printf("%s    %s          7) Alternado             %3.0f%c                          %s  \n", format9, format10, porcentajes[6], '%', format12);
					printf("%s    %s         %s 8) Rebote %s               %3.0f%c                          %s  \n", format9, format10, format11, format1, porcentajes[7], '%',format12);

					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;		
		}

		delayMillis(150);

		if(input == 10){
			tDelays[secuencia] = tDelay;
			porcentajes[secuencia] = porcentaje;
		}	

	}while(input != 113 && input != 81);		
	
	system("setterm -curso on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);
}
