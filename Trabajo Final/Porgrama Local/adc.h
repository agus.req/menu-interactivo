#include <wiringPi.h>
#include <wiringPiI2C.h>

int adc(void){
	int reading;			// Contendrá la lectura del adc
	int fd;				// File descriptor, al arbrir la comunicación I2C
	const char PCF8591 = 0x48;	// Dirección base de este periférico

	if (wiringPiSetup() == -1) 	// En el caso de que exista un error al intentar iniciar la comunicación
		exit(1);		
  
	fd = wiringPiI2CSetup(PCF8591);
	
  		//Se lee ADC0: Potenciómetro
   		wiringPiI2CReadReg8(fd, PCF8591 + 1);  
  		reading = wiringPiI2CReadReg8(fd, PCF8591 + 0); //Lectura en la dirección base, este ADC tiene cuatro canales
   			
   	return reading;	// Se devuelve la lectura hecha
}
