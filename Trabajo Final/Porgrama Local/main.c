#include "EasyPIO.h"		// Necesario para el control de la GPIO (prender o apagar leds)
#include "menuPassword.h"
#include "menuModoTrabajo.h"
#include "menuPrincipal.h"
#include "menuConfigVelocidades.h"
#include "menuSecuencias.h"
#include "menuPrincipalRemoto.h"
#include "menuSecuenciasRemoto.h"
#include <wiringPi.h>		// Necesario para la comunicación serie asíncrona
#include <wiringSerial.h>	// También necesario para la comunicaión serie asíncrona

int tDelays[8] = {0};		// Contendrá los tiempos iniciales de todas las seuencias.
float tiempo_min = 30;		// Corresponde a la velocidad mínima que podrán tener las secuencias, puede ser modificado por otro tiempo_min
float tiempo_max = 2000;	// Corresponde a la velocidad máxima que podrán tener las secuencias, puede ser modificado por otro tiempo_max
float porcentajes[8] = {0};	// Contendrá los porcentajes  inicialesde todas las secuencias, los números que contiene son enteros, pero se lo utiliza como de punto
				// flotante para poder configurar el formato como por ejemplo que este justificado a la derecha, etc en los menúes
int fd = 0;			// File descriptor devuelto por la función serialOpen al abrir el puerto serie

int leyendaConfigVel = 0;

void main(void){
	int modo = 0;		// Indica si es (0)Modo Local, (1)Modo Remoto, (2)Configuración de Velocidades Iniciales
	int secuencia = 0;	// Indica la secuencia de luces seleccionada, hay 8 secuencias disponibles
	int secuenciaRemota = 0;	// Cumple la misma función que la variable de arriba pero para secuencias ejecutadas en Modo Remoto
	int acceso = menuPassword();	// Indica si se  ha permitido el acceso al sistema o no (0)Acceso Denegado, (1)Acceso Permitido
	int salida = 0;			// Cuando salida es 113, indica que se ha salido del menúPrincipal
	
	GPIOConfig();		// Se llama esta función de la biblioteca GPIOConfig.h para setear los pines de GPIO como entrada o salida
	pioInit();		// Necesario para utilizar la biblioteca EasyPIO.h

	apagarLuces();		// Se apagan todos los leds al inciar el programa por seguridad

	for(int i = 0; i < 8; i++){
		tDelays[i] = tiempo_min;	// Se inicializan todas las velocidades al 0% al iniciar el programa
		porcentajes[i] = 0;		// Por lo tanto, es necesario, setear los valores iniciales mediante el potenciómetro cada vez
	}					// que se inicia el programa

	fd = serialOpen("/dev/ttyS0", 9600);	// Se abre el puerto serie, con baud rate de 9600 bauds, el puerto serie es un archivo que se encuentra en /dev/
	wiringPiSetup();			// Se llama a esta función utilizada para setear configuraciones de la bibliteca de Wiring Pi

	do{
		secuencia = 0;			// Se ponen a 0 todas las variables
		secuenciaRemota = 0;
		salida = 0;

		if(acceso)				// Si el acceso es permitido
			modo = menuModoTrabajo();	// Se accede al menú de modo de trabajo hasta que se elija una opción y esta función
							// devuelva el modo de trabajo
		if(acceso && modo == 0){		// Si el modo de trabajo es LOCAL, se entra al menúPrincipal local y queda ahí hasta que se elija una secuencia
			do{				
				secuencia = menuPrincipal();
			
				switch(secuencia){	// Una vez que se eligió una secuencia, se llama a la función que ejecute esa secuencia
					case 0:
						autoFantastico();
						break;
					case 1:
						elChoque();
						break;
					case 2:
						laApilada();
						break;
					case 3:
						laCarrera();
						break;
					case 4: 
						contadorBinario();
						break;
					case 5:
						penduloNewton();
						break;
					case 6:
						alternado();
						break;
					case 7:
						rebote();
						break;	
					default:
						break;
				}			// Si se llegó a este punto, significa que se salió de la función secuencia apretando 'q'
			}while(secuencia != 113);	// Se vuelve al menúPrincipal, mientras no se aprete 'q' queda en este menú
			salida = 113;			// Si se salió sel menú principal salida = 113
		}	
		if(acceso && modo == 1){		// Si se eligió el Modo Remoto, sucede todo lo mismo que en Modo Local
			do{
				secuenciaRemota = menuPrincipalRemoto();
			
				switch(secuenciaRemota){
					case 0:
						autoFantasticoRemoto();
						break;
					case 1:
						elChoqueRemoto();
						break;
					case 2:
						laApiladaRemoto();
						break;
					case 3:
						laCarreraRemoto();
						break;
					case 4: 
						contadorBinarioRemoto();
						break;
					case 5:
						penduloNewtonRemoto();
						break;
					case 6:
						alternadoRemoto();
						break;
					case 7:
						reboteRemoto();
						break;	
					default:
						break;
				}
			}while(secuenciaRemota != 113);	
			salida = 113;		
		}
		if(acceso && modo == 2){		// Si se ha elegido configurar las velocidades iniciales
			menuConfigVelocidades(tDelays);	// Se llama a la función para configurarlas
			salida = 113;
		}	
		if(acceso && modo == 113)		// En el caso de que estando en el menú de Modo de Trabajo, se presione 'q' para salir
			break;				// se sale del búcle do While con este break, se ejecuta la siguiente instrucción luego del do While
	}while(salida == 113);	

	apagarLuces();		// Se apagan todos los leds antes de finalizar el programa por seguridad
}				// se sale del búcle do While con este break, se ejecuta la siguiente instrucción luego del do While




