#include "EasyPIO.h"
#include "GPIOConfig.h"

void apagarLuces(void)
{
	int led[] = {led0, led1, led2, led3, led4, led5, led6, led7};
	int i;
	for(i = 0; i < 8; i++)
		digitalWrite(led[i], 0);
}

void Auto_Fantastico(int first){
	int ledArray[] = {led0, led1, led2, led3, led4, led5, led6, led7}; 
	
	int cond=1;
	static int i=0, j=0;
	
	if(j==2||first)
	{
		for(; i<8 && cond; i++)
			digitalWrite(ledArray[i], 0);
		j=0;
		i=0;
	}
	if(j==0)
	{
		for(; i<8 && cond; i++)
		{
				if(i > 0)
					digitalWrite(ledArray[i-1], 0);
				digitalWrite(ledArray[i], 1);			
				cond=0;
				if(i==7)
				{
					j=1;
					i=6;
				}
		}
	}
	else 
	{ 
		for(; i >= 0 && cond; i--){
			if(i < 7)
				digitalWrite(ledArray[i+1], 0);
			digitalWrite(ledArray[i], 1);
			cond=0;
			
			if(i==0)
			{
				j=2;
			}
		}
	}
}	

void La_Carrera(int first){
	int led[] = {led0, led1, led2, led3, led4, led5, led6, led7};

	int light[2][15] = { 
				{led7, led7, led6, led6, led5, led5, led4, led4, led3, led3, led2, led2, led1, led1, led0},
		        	{0, 0, 0, 0, 0, 0, 0, led7, led6, led5, led4, led3, led2, led1, led0} 
			};
	int cond=1;
	static int i=0, j=0;

	if(i>=16||first)
	{
		for(i = 0; i < 8; i++)
			digitalWrite(led[i], 0);
		i=0;
		j=0;
	}
	
	for(; i<16 && cond; i++){
		if(i > 0){
			digitalWrite(light[0][i-1], 0);
			digitalWrite(light[1][i-1], 0);
		}

		digitalWrite(light[0][i], 1);
		if(light[1][i] != 0)
			digitalWrite(light[1][i], 1);
		cond=0;
	}
}

void El_Choque(int first){
	int led[] = {led0, led1, led2, led3, led4, led5, led6, led7};
	int cond=1;
	static int i=0,j=0;
	int choque[10][8]={
						{0,0,0,0,0,0,0,0},
						{1,0,0,0,0,0,0,1},
						{0,1,0,0,0,0,1,0},
						{0,0,1,0,0,1,0,0},
						{0,0,0,1,1,0,0,0},
						{0,0,0,1,1,0,0,0},
						{0,0,1,0,0,1,0,0},
						{0,1,0,0,0,0,1,0},
						{1,0,0,0,0,0,0,1},
						{0,0,0,0,0,0,0,0} };
	if(j>=10||first)
		j=0;
	if(i>=8||first)
		i=0;

	for(; j<10 && cond; j++)
	{
		for(; i<8 && cond; i++)
		{
			digitalWrite(led[i], choque[j][i]);
		}
		cond=0;
	}
}

void La_Apilada(int first){
	int led[] = {led0, led1, led2, led3, led4, led5, led6, led7};
	int cond=1;
	static int i=0,j=0,k=0;
	
	if(first||k==2)
	{
		for(i=0;i<8;i++)
		{
			digitalWrite(led[i], 0);
		}
		i=0;
		j=0;
		k=0;
	}
	
	if(k==0)
	{
		for(;i<8-j && cond;i++)
		{
			if(i>0)
			{
				digitalWrite(led[i-1], 0);
			}
			digitalWrite(led[i], 1);
			if(i>=7-j)
			{
				k=1;
				i=0;
			}
			cond=0;
		}
	}
	else 
	{
		for(;i<6 && cond;i++)
		{
			if((i % 2)==0)
				digitalWrite(led[7-j], 0);
			else
				digitalWrite(led[7-j], 1);
				
			cond=0;
			if(i==5)
			{
				if(j==7)
					k=2;
				else
				{
					k=0;
					i=-1;
					j++;
				}
			}
		}
	}
}

void Binario(int first)
{
	int led[] = {led7, led6, led5, led4, led3, led2, led1, led0};
	int cond=1;
	static int i=0,j=0,k=0;
	int arr[8]={0};
	int temp[8]={0};
	
	if(first||k==256){
		k=0;
	}
	
	for(i=0;i<8;i++) 
	{
		if(i==0)
			temp[i]=k;
		arr[7-i]=temp[i]%2;
		if(i<7)
			temp[i+1]=temp[i]/2;
	}	
	
	for(i = 0; i < 8; i++)
		digitalWrite(led[i], arr[i]);
		
	
	k++;
}

void Pendulo_Newton(int first){
	int led[] = {led0, led1, led2, led3, led4, led5, led6, led7};
	int cond=1;
	static int i=0,j=0;
	int pend[10][8]={
						{0,1,0,0,1,1,0,0},
						{0,0,1,0,1,1,0,0},
						{0,0,0,1,1,1,0,0},
						{0,0,0,1,1,0,1,0},
						{0,0,0,1,1,0,0,1},
						{0,0,0,1,1,0,0,1},
						{0,0,0,1,1,0,1,0},
						{0,0,0,1,1,1,0,0},
						{0,0,1,0,1,1,0,0},
						{0,1,0,0,1,1,0,0} };
	if(j>=10||first)
		j=0;
	if(i>=8||first)
		i=0;

	for(; j<10 && cond; j++)
	{
		for(; i<8 && cond; i++)
		{
			digitalWrite(led[i], pend[j][i]);
		}
		cond=0;
	}
}

void Alterno(int first)
{
	int led[] = {led0, led1, led2, led3, led4, led5, led6, led7};
	int cond=1;
	static int i=0,j=0;
	int alt[14][8]={
						{1,0,0,0,0,0,0,0},
						{0,0,0,0,0,0,0,1},
						{0,1,0,0,0,0,0,0},
						{0,0,0,0,0,0,1,0},
						{0,0,1,0,0,0,0,0},
						{0,0,0,0,0,1,0,0},
						{0,0,0,1,0,0,0,0},
						{0,0,0,0,1,0,0,0},
						{0,0,0,1,0,0,0,0},
						{0,0,0,0,0,1,0,0},
						{0,0,1,0,0,0,0,0},
						{0,0,0,0,0,0,1,0},
						{0,1,0,0,0,0,0,0},
						{0,0,0,0,0,0,0,1}, };
	if(j>=14||first)
		j=0;
	if(i>=8||first)
		i=0;

	for(; j<14 && cond; j++)
	{
		for(; i<8 && cond; i++)
		{
			digitalWrite(led[i], alt[j][i]);
		}
		cond=0;
	}
}

void Rebote(int first)
{
	int led[] = {led0, led1, led2, led3, led4, led5, led6, led7};
	int cond=1;
	static int i=0,k=0;
	static int a=8;
	
	if(first||a<=0)
	{
		for(i=0;i<8;i++)
		{
			digitalWrite(led[i], 0);
		}
		i=0;
		k=0;
		a=8;
	}
	switch(k){
		case 0:
			for(;i<a && cond;i++)
			{
				if(i==0){
					digitalWrite(led[i], 1);
				}
				else if(i<a){
					digitalWrite(led[i-1], 0);
					digitalWrite(led[i], 1);
				}
				cond=0;
			}
			if(i==a)
			{
				i--;
				k=1;
			}
			break;
		case 1:
			for(;i>0 && cond;i--){
				digitalWrite(led[i-1], 1);
				digitalWrite(led[i], 0);
				cond=0;
			}
			if(i==0)
			{
				i=1;
				k=2;
			}
			break;		
		case 2:
			for(;i<a && cond;i++){
				digitalWrite(led[i-1], 0);
				digitalWrite(led[i], 1);
				cond=0;
			}
			if(i==a)
			{
				a--;
				k=0;
				i=0;
			}
			break;
		default:
			break;
	}
}

void luces(int sel, int prim, int Del)
{
	static int cont=0, i=0;
	if(i>=Del)
	{
		switch(sel){
			case 1:
				Auto_Fantastico(prim);
				break;
			case 2:
				El_Choque(prim);
				break;
			case 3:
				La_Apilada(prim);
				break;
			case 4:
				La_Carrera(prim);
				break;
			case 5:
				Binario(prim);
				break;
			case 6:
				Pendulo_Newton(prim);
				break;
			case 7:
				Alterno(prim);
				break;
			case 8:
				Rebote(prim);
				break;
			default:
				break;
			}
		//cont++;
		//printf("\nCiclo %d",cont);
		i=0;
	}
	else 
		i++;
	delayMillis(1);
}
