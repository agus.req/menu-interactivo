#include <stdlib.h>
#include <stdio.h>
#include <termios.h>
#include <errno.h>
#include <string.h>
#include "EasyPIO.h"

#define FD_STDIN 0
#define format5 "\e[30;47m"	// Fondo
#define format6 "\e[97;100m"	// Cuadro
#define format7 "\e[91;107m"	// Seleccionado
#define format8 "\e[30;40m"	// Sombra

int menuModoTrabajo(void){
	struct termios t_old, t_new;	
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");

	int i;
	int modo = 0;
	int salir = 0;
	char input;
	int aux = 0

	system("echo \"\e[47m\"");
	system("clear");
	printf("\n\n\n\n\n\n\n\n\n\n\n\n");
	printf("%s    %s                                                                 \n", format5, format6);
	printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MODO DE TRABAJO \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format5, format6, format8);
	printf("%s    %s                                                                 %s  \n", format5, format6, format8);
	printf("%s    %s         %s 1) Modo Local %s                                         %s  \n", format5, format6, format7, format1, format8);
	printf("%s    %s          2) Modo Remoto                                         %s  \n", format5, format6, format8);
	printf("%s    %s                                                                 %s  \n", format5, format6, format8);
	printf("%s    %s                                                                 %s  \n", format5, format6, format8);
	printf("%s    %s                                                                 %s  \n", format5, format6, format8);
	printf("%s    %s                                                                 %s  \n", format5, format6, format8);
	printf("%s    %s                                                                 %s  \n", format5, format6, format8);
	printf("%s    %s          3) Configurar Velocidades Iniciales                    %s  \n", format5, format6, format8);
	printf("%s    %s                                                                 %s  \n", format5, format6, format8);
	printf("%s      %s                                                                 \n", format5, format8);
	
	do{
		if(aux == 0)	// aux se pone en 1 cuando se pregunta si se quiere salir y se elije que NO, para que la pantalla se refresque y el getchar() no bloquee la secuencia del programa
			input = getchar();
		aux = 0;

		if(input == 'A')
			modo--;
		if(input == 'B')
			modo++;
		if(modo > 2)
			modo = 2;
		if(modo < 0)
			modo = 0;
			
		switch(modo){
			case 0:		
					system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format5, format6);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MODO DE TRABAJO \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s         %s 1) Modo Local %s                                         %s  \n", format5, format6, format7, format1, format8);
					printf("%s    %s          2) Modo Remoto                                         %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s          3) Configurar Velocidades Iniciales                    %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s      %s                                                                 \n", format5, format8);
					break;
			case 1:		
					system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format5, format6);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MODO DE TRABAJO \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s          1) Modo Local                                          %s  \n", format5, format6, format8);
					printf("%s    %s         %s 2) Modo Remoto %s                                        %s  \n", format5, format6, format7, format1, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s          3) Configurar Velocidades Iniciales                    %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s      %s                                                                 \n", format5, format8);
					break;
			case 2:		
					system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format5, format6);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MODO DE TRABAJO \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s          1) Modo Local                                          %s  \n", format5, format6, format8);
					printf("%s    %s          2) Modo Remoto                                         %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s    %s         %s 3) Configurar Velocidades Iniciales %s                   %s  \n", format5, format6, format7, format1, format8);
					printf("%s    %s                                                                 %s  \n", format5, format6, format8);
					printf("%s      %s                                                                 \n", format5, format8);
					break;


		}
		
		if(input == 81 || input == 113){
			modo = 0;

			system("echo \"\e[47m\"");
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format5, format6);
			printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MODO DE TRABAJO \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format5, format6, format8);
			printf("%s    %s                                                                 %s  \n", format5, format6, format8);
			printf("%s    %s                                                                 %s  \n", format5, format6, format8);
			printf("%s    %s                  ¿Está seguro que desea salir?                  %s  \n", format5, format6, format8);
			printf("%s    %s                                                                 %s  \n", format5, format6, format8);
			printf("%s    %s                                                                 %s  \n", format5, format6, format8);
			printf("%s    %s                                                                 %s  \n", format5, format6, format8);
			printf("%s    %s                                                                 %s  \n", format5, format6, format8);
			printf("%s    %s                                                                 %s  \n", format5, format6, format8);
			printf("%s    %s            %s No %s                                   Sí            %s  \n", format5, format6, format7, format6, format8);
			printf("%s    %s                                                                 %s  \n", format5, format6, format8);
			printf("%s      %s                                                                 \n", format5, format8);

			do{
				input = getchar();
				if(input == 'D')
					salir--;
				if(input == 'C')
					salir++;
				if(salir > 1)
					salir = 1;
				if(salir < 0)
					salir = 0;
			
				switch(salir){
					case 0:		
							system("echo \"\e[47m\"");
							system("clear");
							printf("\n\n\n\n\n\n\n\n\n\n\n\n");
							printf("%s    %s                                                                 \n", format5, format6);
							printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MODO DE TRABAJO \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format5, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s    %s                  ¿Está seguro que desea salir?                  %s  \n", format5, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s    %s            %s No %s                                   Sí            %s  \n", format5, format6, format7, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s      %s                                                                 \n", format5, format8);
							break;
					case 1:		
							system("echo \"\e[47m\"");
							system("clear");
							printf("\n\n\n\n\n\n\n\n\n\n\n\n");
							printf("%s    %s                                                                 \n", format5, format6);
							printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MODO DE TRABAJO \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format5, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s    %s                  ¿Está seguro que desea salir?                  %s  \n", format5, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s    %s             No                                   %s Sí %s           %s  \n", format5, format6, format7, format6, format8);
							printf("%s    %s                                                                 %s  \n", format5, format6, format8);
							printf("%s      %s                                                                 \n", format5, format8);
							break;
					}
			}while(input != 10);
			
			if(salir == 1){
				input = 113;
				break;
			}	
			if(salir == 0){
				input = 0;
				aux = 1;
			}	
		}	

		delayMillis(1);
	}while(input != 10 && input != 81 && input != 113);	
	
	tcsetattr(FD_STDIN, TCSANOW, &t_old);
	system("setterm -cursor on");
	system("echo \"\e[0m\"");
	system("clear");
	
	if(input == 81 || input == 113)
		return 113;
	else	
		return modo;
}
