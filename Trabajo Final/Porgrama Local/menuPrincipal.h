#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <errno.h>
#include "EasyPIO.h"

#define FD_STDIN 0
#define format9 "\e[30;47m"	// Fondo
#define format10 "\e[97;100m"	// Cuadro
#define format11 "\e[91;107m"	// Seleccionado
#define format12 "\e[30;40m"	// Sombra

int menuPrincipal(){
	int i;
	int secuencia = 0;	// Al llamar esta función, se muestra el menúPrincipal y queda seleccionado siempre primero la primer secuencia
	char input;	// Aquí se guardará lo que se presione en el teclado de la Raspberry

	/* Configuración de la Terminal */
	struct termios t_old, t_new;	
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");	
	
	system("echo \"\e[47m\"");
	system("clear");
	printf("\n\n\n\n\n\n\n\n\n\n\n\n");
	printf("%s    %s                                                                 \n", format9, format10);
	printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
	printf("%s    %s                                                                 %s  \n", format9, format10, format12);
	printf("%s    %s         %s 1) Auto Fantástico %s                                    %s  \n", format9, format10, format11, format1, format12);
	printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
	printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
	printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
	printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
	printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
	printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
	printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
	printf("%s    %s                                                                 %s  \n", format9, format10, format12);
	printf("%s      %s                                                                 \n", format9, format12);
	
	do{
		input = getchar();	// Se lee el teclado

		if(input == 'A')	// Si se oprime la flecha hacia arriba
			secuencia--;	// La secuencia disminuye, subiendo la secuencia seleccionada
		if(input == 'B')	// Si se oprime la flecha hacia abajo
			secuencia++;	// La secuencia aumenta, bajando la secuencia seleccionada

		if(secuencia > 7)
			secuencia = 7;
		if(secuencia < 0)
			secuencia = 0;
		
		// Dependiendo de la secuencia seleccionada se muestran distintas situaciones resaltando la secuencia seleccionada	
		switch(secuencia){	
			case 0:		
					system("echo \"\e[47m\"");		
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s         %s 1) Auto Fantástico %s                                    %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);		
					printf("%s      %s                                                                 \n", format9, format12);							
					break;
			case 1:		
					system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s         %s 2) El Choque %s                                          %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 2:		
					system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10,format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s         %s 3) La Apilada %s                                         %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 3:		
					system("echo \"\e[47m\"");		
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s         %s 4) La Carrera %s                                         %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 4:		
					system("echo \"\e[47m\"");		
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s         %s 5) Contador Binario %s                                   %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);							
					break;
			case 5:		
					system("echo \"\e[47m\"");	
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10,format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s         %s 6) Péndulo de Newton %s                                  %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 6:		
					system("echo \"\e[47m\"");	
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s         %s 7) Alternado %s                                          %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 7:		
					system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s         %s 8) Rebote %s                                             %s  \n", format9, format10, format11, format1, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;		
		}

		delayMillis(1);
	}while(input != 113 && input != 81 && input != 10);	// Se sale del do While en caso de que se oprima salir ('q' o 'Q') o Enter		
	
	/* Se reestablece la configuración de la Terminal */
	system("setterm -cursor on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);

	if(input == 113 || input == 81)	// En el caso de que se haya decidido salir, se retorna 113 que indica 'q'
		return 113;
	if(input == 10)			// En  el caso de que se haya apretado Enter, se retorna la secuencia seleccionada
		return secuencia;
}
