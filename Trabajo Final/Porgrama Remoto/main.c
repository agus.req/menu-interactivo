#include "menuPrincipalRemoto.h"
#include "menuSecuenciasRemoto.h"

void main(void){
	int secuenciaRemota;
	int salir;

	do{
		secuenciaRemota = 0;
		salir = 0;
			
		secuenciaRemota = menuPrincipalRemoto();	// Se llama al menu de secuencia remota y va a quedar esperando hasta
								// que se elija una secuencia, la función devuelve la secuencia elegida
		switch(secuenciaRemota){
			case 0:
				autoFantasticoRemoto();
				break;
			case 1:
				elChoqueRemoto();
				break;
			case 2:
				laApiladaRemoto();
				break;
			case 3:
				laCarreraRemoto();
				break;
			case 4: 
				contadorBinarioRemoto();
				break;
			case 5:
				penduloNewtonRemoto();
				break;
			case 6:
				alternadoRemoto();
				break;
			case 7:
				reboteRemoto();
				break;	
			default:
				break;
		}
		
		if(secuenciaRemota >= 0 && secuenciaRemota <= 7)	// Se ejecuta si anteriormente se había entrado en alguna secuencia del 
			salir = 113;					// switch, como salió del switch, significa que se ha vuelto al menú
									// principal, se cumple la condición del do while para llamar a menuPrincipal de nuevo
	}while(secuenciaRemota != 113 || salir == 113);		// si menuPrincipal devuelve 113, quiere decir que en este menu se presionó para
									// salir.
}					
