#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <errno.h>
#include <unistd.h>

#define FD_STDIN 0
#define format9 "\e[30;47m"	// Fondo
#define format10 "\e[97;100m"	// Cuadro
#define format11 "\e[91;107m"	// Seleccionado
#define format12 "\e[30;40m"	// Sombra
		
char getChar2(){					// Función actualizada de getchar para que no sea bloqueante
	char buff[2];					// No alcanza con solo modificar la configuración de termios
	int l = read(STDIN_FILENO, buff, 1);		// porque solo con eso no toma caracteres del buffer de entrada
	if(l>0)					// gits.github.com/whyrusleeping/3983293
		return buff[0];
	return(EOF);
}

void autoFantasticoRemoto(){
	float porcentaje = 0;			// Se carga el porcentaje con el valor inicializado con el potenciómetro
	char input = 0;			// Entrada desde la computadora, es decir, este programa que corre en la computadora
	char inputRasp = 0;			// Entrada desde la raspberry, la raspberry en modo remoto solo puede manda 'q' indicando que salió o los datos
						// que indican el procentaje de velocidad para que pueda ser visto en este programa.
	char inputs[10] = {0};			// Se carga con lo que llegue de la Raspberry, el único caracter que debería llegar es 'q'
						// En modo remoto, la Raspberry solo puede elegir salir del menúPrincipalRemoto y no puede controlar más que eso
						// debido a que en modo remoto, el control lo tiene la computadora.
	
	/* Configuración de la Terminal */
	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");
	
	int fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY); // Se abre el puerto serie
	
	do{
		read(fd, inputs, 10);
		porcentaje = inputs[1]-48;
	}while(inputs[0] != 'P');
	system("echo 'z' > /dev/ttyUSB0");
	
	do{
		system("echo \"\e[47m\"");
		system("clear");
		printf("\n\n\n\n\n\n\n\n\n\n\n\n");
		printf("%s    %s                                                                 \n", format9, format10);
		printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 Auto Fantástico \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s      %s                                                                 \n", format9, format12);
	
		
		read(fd, inputs, 10);			
		inputRasp = inputs[0];
		input = getChar2();	// Se llama a la actualización de la función getchar() para tomar información del teclado de
					// la computadora
		if(input == 'A'){		// Si se apreta la flecha hacia arriba, el porcentaje se sube, no hace falta que la raspberry envíe de 
			porcentaje +=  5;	// vuelta el porcentaje que cambió en ella porque se supone que están sincronizados, pero puede haber bugs
						// que deben arreglarse
			if(porcentaje <= 105)	// Si se apretó para subir siendo el porcentaje menor o igual a 100 (el límite superior)
				system("echo 'A' > /dev/ttyUSB0");	// se comunica a la Raspberry que se quiere subir la velocidad
		}							// con esto, la Raspberry actualiza su pantalla y da la orden al Hardware
									// para que suba la velocidad
		if(input == 'B'){	// Aquí sucede lo mismo que lo anterior pero para cuando se apreta la flecha hacia abajo para bajar la velocidad
			porcentaje -= 5;
			if(porcentaje >= -5)
				system("echo 'B' > /dev/ttyUSB0");
		}
		
		if(porcentaje  > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;	
		
		usleep(60000);	// Simula un delya() y su parámetro va en microsegundos, este delay es necesario para que
				// no haya constantemente un refresco de la pantalla que cause molestia al usuario
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);	// En el caso de que la computadora o la Raspberry hayan decidido
											// salir, no se cumple la condición del do While y se sale	
	
	/* Se reestablece la configuración vieja que tenía la Terminal */
	system("setterm -cursor on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);
	
	if(input == 113 || input == 81)		// En el caso de que la computadora quiera salir, hay que comunicarle a la Raspberry para
		system("echo 'q' > /dev/ttyUSB0");	// que también salga y vaya al menúPrincipalRemoto
		
	close(fd);	// Se cierra el puerto serie
}

void elChoqueRemoto(){
	float porcentaje = 0;	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	char input = 0;				// Entrada desde la computadora, es decir, este programa que corre en la computadora
	char inputRasp = 0;			// Entrada desde la raspberry, la raspberry en modo remoto solo puede manda 'q' indicando que salió o los datos
						// que indican el procentaje de velocidad para que pueda ser visto en este programa.
	char inputs[10] = {0};
	
	/* Configuración de la Terminal */
	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");
	
	int fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY); // Se abre el puerto serie

	do{
		read(fd, inputs, 10);
		porcentaje = inputs[1]-48;
	}while(inputs[0] != 'P');
	system("echo 'z' > /dev/ttyUSB0"); 

	do{	
		system("echo \"\e[47m\"");
		system("clear");
		printf("\n\n\n\n\n\n\n\n\n\n\n\n");
		printf("%s    %s                                                                 \n", format9, format10);
		printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 El  Choque \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s      %s                                                                 \n", format9, format12);
	

		read(fd, inputs, 10);			// Se lee el puerto serie
		inputRasp = inputs[0];
		input = getChar2();	// Se llama a la actualización de la función getchar()

		if(input == 'A'){
			porcentaje +=  5;
			if(porcentaje <= 105)
				system("echo 'A' > /dev/ttyUSB0");
		}	
		if(input == 'B'){
			porcentaje -= 5;
			if(porcentaje >= -5)
				system("echo 'B' > /dev/ttyUSB0");
		}	
		
		if(porcentaje  > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;
			
		usleep(60000);
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);		
	
	/* Se reestablece la configuración vieja que tenía la Terminal */
	system("setterm -cursor on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);
	
	if(input == 113 || input == 81)
		system("echo 'q' > /dev/ttyUSB0");
		
	close(fd);
}

void laApiladaRemoto(){
	float porcentaje = 0;	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	char input = 0;				// Entrada desde la computadora, es decir, este programa que corre en la computadora
	char inputRasp = 0;			// Entrada desde la raspberry, la raspberry en modo remoto solo puede manda 'q' indicando que salió o los datos
						// que indican el procentaje de velocidad para que pueda ser visto en este programa.
	char inputs[10] = {0};
	
	/* Configuración de la Terminal */
	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");
	
	int fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY); // Se abre el puerto serie
	
	do{
		read(fd, inputs, 10);
		porcentaje = inputs[1]-48;
	}while(inputs[0] != 'P');
	system("echo 'z' > /dev/ttyUSB0");

	do{	
		system("echo \"\e[47m\"");
		system("clear");
		printf("\n\n\n\n\n\n\n\n\n\n\n\n");
		printf("%s    %s                                                                 \n", format9, format10);
		printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 La Apilada \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s      %s                                                                 \n", format9, format12);
	

		read(fd, inputs, 10);			// Se lee el puerto serie
		inputRasp = inputs[0];
		input = getChar2();	// Se llama a la actualización de la función getchar()

		if(input == 'A'){
			porcentaje +=  5;
			if(porcentaje <= 105)
				system("echo 'A' > /dev/ttyUSB0");
		}	
		if(input == 'B'){
			porcentaje -= 5;
			if(porcentaje >= -5)
				system("echo 'B' > /dev/ttyUSB0");
		}
		
		if(porcentaje  > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;
		
		usleep(60000);
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);		
	
	/* Se reestablece la configuración vieja que tenía la Terminal */
	system("setterm -cursor on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);
	
	if(input == 113 || input == 81)
		system("echo 'q' > /dev/ttyUSB0");
		
	close(fd);
}

void laCarreraRemoto(){
	float porcentaje = 0;	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	char input = 0;				// Entrada desde la computadora, es decir, este programa que corre en la computadora
	char inputRasp = 0;			// Entrada desde la raspberry, la raspberry en modo remoto solo puede manda 'q' indicando que salió o los datos
						// que indican el procentaje de velocidad para que pueda ser visto en este programa.
	char inputs[10] = {0};
	
	/* Configuración de la Terminal */
	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");
	
	int fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY); // Se abre el puerto serie
	
	do{
		read(fd, inputs, 10);
		porcentaje = inputs[1]-48;
	}while(inputs[0] != 'P');
	system("echo 'z' > /dev/ttyUSB0");

	do{	
		system("echo \"\e[47m\"");
		system("clear");
		printf("\n\n\n\n\n\n\n\n\n\n\n\n");
		printf("%s    %s                                                                 \n", format9, format10);
		printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 La Carrera \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s      %s                                                                 \n", format9, format12);
	
		
		read(fd, inputs, 10);			// Se lee el puerto serie
		inputRasp = inputs[0];
		input = getChar2();	// Se llama a la actualización de la función getchar()

		if(input == 'A'){
			porcentaje +=  5;
			if(porcentaje <= 105)
				system("echo 'A' > /dev/ttyUSB0");
		}	
		if(input == 'B'){
			porcentaje -= 5;
			if(porcentaje >= -5)
				system("echo 'B' > /dev/ttyUSB0");
		}	
		
		if(porcentaje  > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;
		
		usleep(60000);
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);		
	
	/* Se reestablece la configuración vieja que tenía la Terminal */
	system("setterm -cursor on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);
	
	if(input == 113 || input == 81)
		system("echo 'q' > /dev/ttyUSB0");
		
	close(fd);
}

void contadorBinarioRemoto(){
	float porcentaje = 0;	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	char input = 0;				// Entrada desde la computadora, es decir, este programa que corre en la computadora
	char inputRasp = 0;			// Entrada desde la raspberry, la raspberry en modo remoto solo puede manda 'q' indicando que salió o los datos
						// que indican el procentaje de velocidad para que pueda ser visto en este programa.
	char inputs[10] = {0};
	
	/* Configuración de la Terminal */
	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");
	
	int fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY); // Se abre el puerto serie
	
	do{
		read(fd, inputs, 10);
		porcentaje = inputs[1]-48;
	}while(inputs[0] != 'P');
	system("echo 'z' > /dev/ttyUSB0");

	do{
		system("echo \"\e[47m\"");
		system("clear");
		printf("\n\n\n\n\n\n\n\n\n\n\n\n");
		printf("%s    %s                                                                 \n", format9, format10);
		printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 Contador Binario \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s      %s                                                                 \n", format9, format12);
	
		
		read(fd, inputs, 10);			// Se lee el puerto serie
		inputRasp = inputs[0];
		input = getChar2();	// Se llama a la actualización de la función getchar()

		if(input == 'A'){
			porcentaje +=  5;
			if(porcentaje <= 105)
				system("echo 'A' > /dev/ttyUSB0");
		}	
		if(input == 'B'){
			porcentaje -= 5;
			if(porcentaje >= -5)
				system("echo 'B' > /dev/ttyUSB0");
		}	
		
		if(porcentaje  > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;
		
		usleep(60000);
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);		
	
	/* Se reestablece la configuración vieja que tenía la Terminal */
	system("setterm -cursor on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);
	
	if(input == 113 || input == 81)
		system("echo 'q' > /dev/ttyUSB0");
	
	close(fd);
}

void penduloNewtonRemoto(){
	float porcentaje = 0;	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	char input = 0;				// Entrada desde la computadora, es decir, este programa que corre en la computadora
	char inputRasp = 0;			// Entrada desde la raspberry, la raspberry en modo remoto solo puede manda 'q' indicando que salió o los datos
						// que indican el procentaje de velocidad para que pueda ser visto en este programa.
	char inputs[10] = {0};
	
	/* Configuración de la Terminal */
	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");
	
	int fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY); // Se abre el puerto serie
	
	do{
		read(fd, inputs, 10);
		porcentaje = inputs[1]-48;
	}while(inputs[0] != 'P');
	system("echo 'z' > /dev/ttyUSB0"); 

	do{
		
		system("echo \"\e[47m\"");
		system("clear");
		printf("\n\n\n\n\n\n\n\n\n\n\n\n");
		printf("%s    %s                                                                 \n", format9, format10);
		printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 Péndulo de Newton \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s      %s                                                                 \n", format9, format12);
	
		
		read(fd, inputs, 10);			// Se lee el puerto serie
		inputRasp = inputs[0];
		input = getChar2();	// Se llama a la actualización de la función getchar()

		if(input == 'A'){
			porcentaje +=  5;
			if(porcentaje <= 105)
				system("echo 'A' > /dev/ttyUSB0");
		}	
		if(input == 'B'){
			porcentaje -= 5;
			if(porcentaje >= -5)
				system("echo 'B' > /dev/ttyUSB0");
		}	
		
		if(porcentaje  > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;
		
		usleep(60000);
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);		
	
	/* Se reestablece la configuración vieja que tenía la Terminal */
	system("setterm -cursor on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);
	
	if(input == 113 || input == 81)
		system("echo 'q' > /dev/ttyUSB0");
		
	close(fd);
}

void alternadoRemoto(){
	float porcentaje = 0;	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	char input = 0;				// Entrada desde la computadora, es decir, este programa que corre en la computadora
	char inputRasp = 0;			// Entrada desde la raspberry, la raspberry en modo remoto solo puede manda 'q' indicando que salió o los datos
						// que indican el procentaje de velocidad para que pueda ser visto en este programa.
	char inputs[10] = {0};
	
	/* Configuración de la Terminal */
	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");
	
	int fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY); // Se abre el puerto serie
	
	do{
		read(fd, inputs, 10);
		porcentaje = inputs[1]-48;
	}while(inputs[0] != 'P');
	system("echo 'z' > /dev/ttyUSB0"); 

	do{	
		system("echo \"\e[47m\"");
		system("clear");
		printf("\n\n\n\n\n\n\n\n\n\n\n\n");
		printf("%s    %s                                                                 \n", format9, format10);
		printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 Alternado \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s      %s                                                                 \n", format9, format12);
	
		
		read(fd, inputs, 10);			// Se lee el puerto serie
		inputRasp = inputs[0];
		input = getChar2();	// Se llama a la actualización de la función getchar()

		if(input == 'A'){
			porcentaje +=  5;
			if(porcentaje <= 105)
				system("echo 'A' > /dev/ttyUSB0");
		}	
		if(input == 'B'){
			porcentaje -= 5;
			if(porcentaje >= -5)
				system("echo 'B' > /dev/ttyUSB0");
		}
		
		if(porcentaje  > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;	
		
		usleep(60000);
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);		
	
	/* Se reestablece la configuración vieja que tenía la Terminal */
	system("setterm -cursor on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);
	
	if(input == 113 || input == 81)
		system("echo 'q' > /dev/ttyUSB0");
		
	close(fd);
}

void reboteRemoto(){
	float porcentaje = 0;	// Se carga el porcentaje con el valor inicializado con el potenciómetro
	char input = 0;				// Entrada desde la computadora, es decir, este programa que corre en la computadora
	char inputRasp = {0};			// Entrada desde la raspberry, la raspberry en modo remoto solo puede manda 'q' indicando que salió o los datos 
						// que indican el procentaje de velocidad para que pueda ser visto en este programa.
	char inputs[10] = {0};
	
	/* Configuración de la Terminal */
	struct termios t_old, t_new;	                     		
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ICANON);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");

	int fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY); // Se abre el puerto serie
	
	do{
		read(fd, inputs, 10);
		porcentaje = inputs[1]-48;
	}while(inputs[0] != 'P');
	system("echo 'z' > /dev/ttyUSB0");	 

	do{
		
		system("echo \"\e[47m\"");
		system("clear");
		printf("\n\n\n\n\n\n\n\n\n\n\n\n");
		printf("%s    %s                                                                 \n", format9, format10);
		printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 Rebote \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 %3.0f%c \u2502 %s  \n", format9, format10, porcentaje, '%', format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2191 para Aumentar la Velocidad                  %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione \u2193 para Disminuir la Velocidad                 %s  \n", format9, format10, format12);
		printf("%s    %s        \u2022 Presione 'q' o 'Q' para Volver o Salir                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s    %s                                                                 %s  \n", format9, format10, format12);
		printf("%s      %s                                                                 \n", format9, format12);
		
		
		read(fd, inputs, 10);			// Se lee el puerto serie				
		inputRasp = inputs[0];					
		input = getChar2();	// Se llama a la actualización de la función getchar()

		if(input == 'A'){
			porcentaje +=  5;
			if(porcentaje <= 105)
				system("echo 'A' > /dev/ttyUSB0");
		}	
		if(input == 'B'){
			porcentaje -= 5;
			if(porcentaje >= -5)
				system("echo 'B' > /dev/ttyUSB0");
		}
		
		if(porcentaje  > 100)
			porcentaje = 100;
		if(porcentaje < 0)
			porcentaje = 0;	
		
		usleep(60000);
	}while(input != 113 && input != 81 && inputRasp != 113 && inputRasp != 81);		
	
	/* Se reestablece la configuración vieja que tenía la Terminal */
	system("setterm -cursor on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);
	
	if(input == 113 || input == 81)
		system("echo 'q' > /dev/ttyUSB0");
		
	close(fd);
}

