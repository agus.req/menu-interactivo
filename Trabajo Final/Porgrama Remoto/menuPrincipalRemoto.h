#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>

#define FD_STDIN 0
#define format9 "\e[30;47m"	// Fondo
#define format10 "\e[97;100m"	// Cuadro
#define format11 "\e[91;107m"	// Seleccionado
#define format12 "\e[30;40m"	// Sombra

extern int fd;					// file descriptor, se carga con un valor que devuelve la función que abre /dev/USB0 en el puerto serial

char getChar1(){				// Función actualizada de getchar para que no sea bloqueante
	char buff[2];				// No alcanza con solo modificar la configuración de termios
	int l = read(STDIN_FILENO, buff, 1);	// porque solo con eso no toma caracteres del buffer de entrada
	if(l>0)				// gits.github.com/whyrusleeping/3983293
		return buff[0];
	return(EOF);
}

int menuPrincipalRemoto(){
	int i;
	int salir = 0;
	int aux = 0;
	int secuencia = 0;
	char input = 0;		// Entrada desde la computadora, es decir, este programa que corre en la computadora
	char inputRasp = 0;		// Entrada desde la raspberry, la raspberry en modo remoto solo puede manda 'q' indicando que salió o los datos
					// que indican el procentaje de velocidad para que pueda ser visto en este programa.
	char inputs[10] = {0};		// Se carga con lo que llegue de la Raspberry, el único caracter que debería llegar es 'q'
					// En modo remoto, la Raspberry solo puede elegir salir del menúPrincipalRemoto y no puede controlar más que eso
					// debido a que en modo remoto, el control lo tiene la computadora.
		
	/* Configuración de la Terminal */
	struct termios t_old, t_new;	
	tcgetattr(FD_STDIN, &t_old);
	t_new = t_old;		
	t_new.c_lflag &= ~(ECHO | ECHOE | ECHONL | ICANON | TCIOFF);	
	t_new.c_cc[VMIN] = 0;			// Se agrega esta línea y la de abajo en la configuración de Termios para hacer que getchar()
	t_new.c_cc[VTIME] = 0;			// no sea bloqueante
	tcsetattr(FD_STDIN, TCSANOW, &t_new);
	system("setterm -cursor off");
	
	int fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY); // Se abre el puerto serie, devuelve el file descriptor
	
	/* Primer pantalla que se muestra al entrar en este menú AUTO FANTÁSTICO seleccionado */
	system("echo \"\e[47m\"");
	system("clear");
	printf("\n\n\n\n\n\n\n\n\n\n\n\n");
	printf("%s    %s                                                                 \n", format9, format10);
	printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
	printf("%s    %s                                                                 %s  \n", format9, format10, format12);
	printf("%s    %s         %s 1) Auto Fantástico %s                                    %s  \n", format9, format10, format11, format10, format12);
	printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
	printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
	printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
	printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
	printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
	printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
	printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
	printf("%s    %s                                                                 %s  \n", format9, format10, format12);
	printf("%s      %s                                                                 \n", format9, format12);
	
	do{
		read(fd, inputs, 10);				// Se lee la entrada del puerto serie, se guarda la data en el arreglo inputs[10]
		inputRasp = inputs[0];				// inputRasp recibe el primer caracter recibido, es decir, en la posición 0
		input = getChar1();				// Se lee la entrada por teclado de la computadora

		if(input == 'A'){				// Si en la computadora se va hacia arriba
			secuencia--;				// Se debe ir a la secuencia de arriba en el menú
			system("echo 'A' > /dev/ttyUSB0");	// Hay que indicarle a la raspberry que se fue hacia arriba en la cooputadora
		}						// para que se actualice la pantalla del programa que corre en ella
		if(input == 'B'){				// Si en la computadora se va hacia abajo
			secuencia++;				// Se debe ir a la secuencia de abajo en el menú
			system("echo 'B' > /dev/ttyUSB0");	// Hay que indicarle también que fue hacia abajo
		}
		if(secuencia > 7)		
			secuencia = 7;
		if(secuencia < 0)
			secuencia = 0;
			
		switch(secuencia){	// Dependiendo de la secuencia que esté seleccionada, se pueden mostrar diferentes pantallas
					// que difieren una de otra en que secuencia se está resaltando con color
					
			case 0:	/* AUTO FANTÁSTICO seleccionado */
					system("echo \"\e[47m\"");		
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s         %s 1) Auto Fantástico %s                                    %s  \n", format9, format10, format11, format10, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);							
					break;
			case 1:	/* EL CHOQUE seleccionado */
					system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s         %s 2) El Choque %s                                          %s  \n", format9, format10, format11, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 2:	/* LA APILADA seleccionado */
					system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10,format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s         %s 3) La Apilada %s                                         %s  \n", format9, format10, format11, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 3:	/* LA CARRERA seleccionado */
					system("echo \"\e[47m\"");		
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s         %s 4) La Carrera %s                                         %s  \n", format9, format10, format11, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 4:	/* CONTADOR BINARIO seleccionado */
					system("echo \"\e[47m\"");		
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s         %s 5) Contador Binario %s                                   %s  \n", format9, format10, format11, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);							
					break;
			case 5:	/* PÉNDULO DE NEWTON seleccionado */
					system("echo \"\e[47m\"");	
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10,format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s         %s 6) Péndulo de Newton %s                                  %s  \n", format9, format10, format11, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 6:	/* ALTERNADO seleccionado */ 
					system("echo \"\e[47m\"");	
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s         %s 7) Alternado %s                                          %s  \n", format9, format10, format11, format10, format12);
					printf("%s    %s          8) Rebote                                              %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;
			case 7:	/* REBOTE seleccionado */
					system("echo \"\e[47m\"");
					system("clear");
					printf("\n\n\n\n\n\n\n\n\n\n\n\n");
					printf("%s    %s                                                                 \n", format9, format10);
					printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MENÚ  PRINCIPAL \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s    %s          1) Auto Fantástico                                     %s  \n", format9, format10, format12);
					printf("%s    %s          2) El Choque                                           %s  \n", format9, format10, format12);
					printf("%s    %s          3) La Apilada                                          %s  \n", format9, format10, format12);
					printf("%s    %s          4) La Carrera                                          %s  \n", format9, format10, format12);
					printf("%s    %s          5) Contador Binario                                    %s  \n", format9, format10, format12);
					printf("%s    %s          6) Péndulo de Newton                                   %s  \n", format9, format10, format12);
					printf("%s    %s          7) Alternado                                           %s  \n", format9, format10, format12);
					printf("%s    %s         %s 8) Rebote %s                                             %s  \n", format9, format10, format11, format10, format12);
					printf("%s    %s                                                                 %s  \n", format9, format10, format12);
					printf("%s      %s                                                                 \n", format9, format12);
					break;		
		}
		
		if(input == 81 || input == 113){	// Si se apreta 'q' o 'Q' se pregunta si se quiere salir realmente, es un menú
							// con dos opciones, SI o NO, también hay dos pantallas diferentes en función de 
							// cual se está seleccionando
			system("echo \"\e[47m\"");	
			system("clear");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n");
			printf("%s    %s                                                                 \n", format9, format10);
			printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MODO DE TRABAJO \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                  ¿Está seguro que desea salir?                  %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s    %s            %s No %s                                   Sí            %s  \n", format9, format10, format11, format10, format12);
			printf("%s    %s                                                                 %s  \n", format9, format10, format12);
			printf("%s      %s                                                                 \n", format9, format12);

			do{				// Se entra a un do While para seleccionar la respuesta
				input = getChar1();
				if(input == 'D')	// Si se apreta la flecha izquierda, se selecciona NO
					salir--;
				if(input == 'C')	// sI SE apreta la flecha derecha, se selecciona SI
					salir++;
				if(salir > 1)
					salir = 1;
				if(salir < 0)
					salir = 0;
			
				switch(salir){
					case 0:	system("echo \"\e[47m\"");
							system("clear");
							printf("\n\n\n\n\n\n\n\n\n\n\n\n");
							printf("%s    %s                                                                 \n", format9, format10);
							printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MODO DE TRABAJO \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s    %s                  ¿Está seguro que desea salir?                  %s  \n", format9, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s    %s            %s No %s                                   Sí            %s  \n", format9, format10, format11, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s      %s                                                                 \n", format9, format12);
							break;
					case 1:		system("echo \"\e[47m\"");
							system("clear");
							printf("\n\n\n\n\n\n\n\n\n\n\n\n");
							printf("%s    %s                                                                 \n", format9, format10);
							printf("%s    %s  \u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2524 MODO DE TRABAJO \u251C\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500  %s  \n", format9, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s    %s                  ¿Está seguro que desea salir?                  %s  \n", format9, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s    %s             No                                   %s Sí %s           %s  \n", format9, format10, format11, format10, format12);
							printf("%s    %s                                                                 %s  \n", format9, format10, format12);
							printf("%s      %s                                                                 \n", format9, format12);
							break;
					}
					usleep(50000);	// Simula un delay, el parámetro se ingrea en microsegundos
			}while(input != 10);
			
			if(salir == 1){	// Si se seleccionó que SI
				input = 113;	// Se toma la entrada como 113 para que no se cumpla la condición en el do While y salga
				break;		// break hace que se ejecute la siguiente instrucción después del bucle en el cual se encuentra
			}	
			if(salir == 0){	// Si se selleccionó que NO
				input = 0;	// se toma como entrada para que la condición del do While se cumpla y vuelva a empezar el bucle, 
				aux = 1;	// volviendo al menúPrincipalRemoto
			}	
		}
		usleep(50000);
	}while(input != 113 && input != 81 && input != 10 && inputRasp != 113 && inputRasp != 81);				
									
	/* Se reestablece la configuración vieja que tenía la Terminal */								
	system("setterm -cursor on");
	system("echo \"\e[0m\"");
	system("clear");
	tcsetattr(FD_STDIN, TCSANOW, &t_old);
	
	
	/* Decisiones que se toman para terminar el programa */
	if(input == 113 || input == 81){		// Si se salió porque en la computadora se presionó salir, hay que comunicarle a la raspberry
		system("echo 'q' > /dev/ttyUSB0");	// para que también salga del este menú remoto
		return 113;				// A su vez la función retorna 113 para indicar que debe terminar este programa en la computadora
	}	
	if(inputRasp == 113 || inputRasp == 81)	// Si la raspberry salió, envió una 'q' como dato, no tengo que indicarle de vuelta a la raspbery que 
		return 113;				// salgo porque cuando se apreta 'q' en ella sale por sí sola y es inncesario volverle a comunicar
							// Simplemente la función retorna 113 para que el programa en computadora termine
	if(input == 10){				// Si la entrada era Enter, se retorna el valor de la secuencia seleccionada para ir al otro menú
		system("echo 's' > /dev/ttyUSB0");	// A su vez dee indicarse que se presionó Enter para que el programa que corre en la raspberry
		return secuencia;			// también entre a una secuencia de luces.
	}	
	
	close(fd);	// Se cierra el puerto serie
}
